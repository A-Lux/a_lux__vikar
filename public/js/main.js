(function ($) {
	 $('.close-modal').click(function (e) {
		e.preventDefault();
		$('.feedback, .overlay, .modal-ty').hide();
	  });
	$('.btn-send').click(function (e) {
		e.preventDefault();
		$('.modal-ty , .overlay').show();
	  });
  new WOW().init();

  $(document).ready(function () {
    $('.date').mask('00/00/0000');
    $('.time').mask('00:00:00');
    $('.date_time').mask('00/00/0000 00:00:00');
    $('.cep').mask('00000-000');
    $('.phone').mask('0000-0000');
    $('.phone_with_ddd').mask('(00) 0000-0000');
    $('.phone_us').mask('+7(000) 000-0000');
    $('.mixed').mask('AAA 000-S0S');
    $('.cpf').mask('000.000.000-00', { reverse: true });
    $('.cnpj').mask('00.000.000/0000-00', { reverse: true });
    $('.money').mask('000.000.000.000.000,00', { reverse: true });
    $('.money2').mask("#.##0,00", { reverse: true });
    $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
      translation: {
        'Z': {
          pattern: /[0-9]/, optional: true
        }
      }
    });
    $('.ip_address').mask('099.099.099.099');
    $('.percent').mask('##0,00%', { reverse: true });
    $('.clear-if-not-match').mask("00/00/0000", { clearIfNotMatch: true });
    $('.placeholder').mask("00/00/0000", { placeholder: "__/__/____" });
    $('.fallback').mask("00r00r0000", {
      translation: {
        'r': {
          pattern: /[\/]/,
          fallback: '/'
        },
        placeholder: "__/__/____"
      }
    });
    $('.selectonfocus').mask("00/00/0000", { selectOnFocus: true });
  });

  /* OffCanvas Menu
   * ------------------------------------------------------ */
  var clOffCanvas = function () {

    var menuTrigger = $('.header-menu-toggle'),
      nav = $('.header-nav'),
      closeButton = nav.find('.header-nav__close'),
      siteBody = $('body'),
      mainContents = $('section, footer');

    // open-close menu by clicking on the menu icon
    menuTrigger.on('click', function (e) {
      e.preventDefault();
      // menuTrigger.toggleClass('is-clicked');
      siteBody.toggleClass('menu-is-open');
    });

    // close menu by clicking the close button
    closeButton.on('click', function (e) {
      e.preventDefault();
      menuTrigger.trigger('click');
    });

    // close menu clicking outside the menu itself
    siteBody.on('click', function (e) {
      if (!$(e.target).is('.header-nav, .header-nav__content, .header-menu-toggle, .header-menu-toggle span')) {
        // menuTrigger.removeClass('is-clicked');
        siteBody.removeClass('menu-is-open');
      }
    });

  };

  /* Initialize
   * ------------------------------------------------------ */
  (function ssInit() {
    clOffCanvas();
  })();

  // ORDER CALC
  $('.down').click(function (e) {
    e.preventDefault()
    if ($(this).next().val() > 1) {
      if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
  });

  $('.up').click(function (e) {
    e.preventDefault()
    if ($(this).prev().val() < 10) {
      $(this).prev().val(+$(this).prev().val() + 1);
    }
  });
  // END ORDER CALC

// MAIN CALLBACK
  var callbackBtn = $('.main-callback-message-btn'),
      callbackBlock = $('.main-callback-block'),
      callbackClose = $('.main-callback-close');

  $(callbackBlock).hide().css('transform', 'translateX(100%)')
  $(callbackBtn).on('click', function(e) {
    e.preventDefault()
    $(callbackBlock).show().css('transform', 'translateX(0%)')
  })

  $(callbackClose).on('click', function() {
    $(callbackBlock).css('transform', 'translateX(100%)')
  })

// END MAIN CALLBACK

})(jQuery);

function add_to_cart(id){
    $.ajax({
        type: "GET",
        url: '/order/addToCart',
        data: {
            productId:id,
        },
        success: function()
        {
            window.location.href = '/order';
        },
        error: function(msg){
            console.log('error');
        }
    });
}
// CALC FOTORAMA
$(function() {

    $('.fotorama').fotorama({
        allowfullscreen: true,
        nav: 'thumbs',
        autoplay: true,
    });

});
// END CALC FOTORAMA

// CHECKBOX MAGNIFIC
$(function(){
    $('.checkbox-block').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
        zoom: true
    });
});
// END CHECKBOX MAGNIFIC

	// GALLERY MAGNIFIC
$(function(){
    $('.gallery-content').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
        zoom: true
    });
});
	// END GALLERY MAGNIFIC



$('.card-table').find('table').addClass('table table-bordered table-striped');
$('.table.table-bordered.table-striped').removeAttr('style');
$('.table.table-bordered.table-striped').find('tr').removeAttr('style');
$('.table.table-bordered.table-striped').find('td').removeAttr('style');
// $('.table.table-bordered.table-striped').find('tr:first td').addClass('fff').removeAttr('td').attr('th');
$('.table.table-bordered.table-striped').find('tr:first').attr('style', 'background-color: #ee1d25;color:#fff;');
/*$('.table.table-bordered.table-striped').find('tr:first td').replaceWith(function(index, oldHTML){
    return $("<th>").html(oldHTML);
});*/

var price_order = '';
var cena_ed = '';
//$('.inc0 tr td:nth-child(n+2):nth-child(-n+5)').on('click', function(){
$('.inc0 tr td').on('click', function(){
    if(!isNaN($('.col_order').val() * $(this).html())) {
        $('.inc0 tr td').css('background-color', 'rgba(0, 0, 0, 0.05)');
        $(this).css('background-color', '#ee1d25');
        price_order = $(this).html();
        if($('.p_banner').is(':checked')){
            cena_ed = $(this).html();
            $('.cena_ed').html($(this).html());
            $('.cena_ob').html($('.col_order').val() * $(this).html());
        }
        $('.pay_order').show();
        itog();
    }
});

$('.col_order').on('keyup', function(){
    $('.cena_ed').html(price_order);
    $('.cena_ob').html($('.col_order').val() * price_order);
    itog();
});

$('.order-btn').on('click', function() {
    if($(this).is(':checked')) {
        $('.feedback').show();
        $(".overlay").show();
    }else{
        $('.feedback').hide();
        $(".overlay").hide();
        $(".dostavka").html('');
        itog();
    }
});

$('.p_banner').on('click', function() {
    if($(this).is(':checked')){
        $('.cena_ed').html(cena_ed);
        $('.cena_ob').html($('.col_order').val() * cena_ed);
    }else{
        $('.cena_ed').html('');
        $('.cena_ob').html('');
    }
    itog();
});

$('.u_dizainera').on('click', function() {
    if($(this).is(':checked')) {
        $('.c_dizainera').html(3000);
        $('.c_dizainera_ed').html(3000);
    }else{
        $('.c_dizainera').html('');
        $('.c_dizainera_ed').html('');
    }
    itog();
});

var itog_address = '';

$('.end_map').on('click', function() {
    $('.order-btn').attr('checked', 'checked');
    $('.feedback').hide();
    $(".overlay").hide();
    $('.dostavka').html(itog_address);
    itog();
});

function itog(){
    var banner = 0;
    if($('.p_banner').is(':checked'))
        banner = !isNaN(parseInt($('.cena_ob').html()))? parseInt($('.cena_ob').html()):0;

    var u_dizainera = 0;
    if($('.u_dizainera').is(':checked'))
        u_dizainera = !isNaN(parseInt($('.c_dizainera').html()))? parseInt($('.c_dizainera').html()):0;

    var dostavka = 0;
    if($('.order-btn').is(':checked'))
        dostavka = !isNaN(parseInt($('.dostavka').html()))? parseInt($('.dostavka').html()):0;

    $('.itog').html(banner + u_dizainera + dostavka);

    var col_order = $('.col_order').val();

    $.ajax({
        type: "GET",
        url: '/order/orderitog',
        data: {
            productId: $('.model_id').val(),
            col_order: col_order,
            cena_ob: cena_ed,
            u_dizainera: u_dizainera,
            dostavka: dostavka,
            adres: $('.address').val()
        },
        success: function(rez)
        {
            //alert(rez);
        },
        error: function(msg){
            console.log('error');
        }
    });
}

$('body').on('change', '.calc-bg input[type="radio"]', function(){
    itog_price();
});

$('body').on('click', '.btn-operation', function(){
    itog_price();
});

$('body').on('keyup', '.quantity', function(){
    itog_price();
});

function itog_price(){
    var price = 0;
    $('.calc-bg input[type="radio"]').each(function(){
        if(!isNaN(parseInt($(this).attr('data-val'))) && $(this).is(':checked'))
            price += parseInt($(this).attr('data-val'));
    });
    var quantity_sm = parseInt($('.quantity_sm').val()) * parseInt($('input[name="price_sm"]').val());
    var quantity_col = parseInt($('.quantity_col').val()) * parseInt($('input[name="price_col"]').val());

    price += quantity_sm + quantity_col;

    $('.price-button span').html(price + ' тг.');
}

$('body').on('click', '.oformit_zacaz2', function(e){
    e.preventDefault();
    $(this).closest('form').submit();
});



function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

var images1 = {
  img1: ['url(/public/images/main-layer-1.jpg)', 'url(/public/images/main-layer-2.jpg)', 'url(/public/images/main-layer-3.jpg)', 'url(/public/images/main-layer-4.jpg)' , 'url(/public/images/main-layer-5.jpg)', 'url(/public/images/main-layer-6.jpg)' ],
  img2: ['url(/public/images/main-layer-8.jpg)', 'url(/public/images/main-layer-9.jpg)', 'url(/public/images/main-layer-10.jpg)', 'url(/public/images/main-layer-11.jpg)' , 'url(/public/images/main-layer-12.jpg)', 'url(/public/images/main-layer-13.jpg)' ],
  img3: ['url(/public/images/main-layer-5.jpg)', 'url(/public/images/main-layer-6.jpg)', 'url(/public/images/main-layer-7.jpg)', 'url(/public/images/main-layer-8.jpg)' , 'url(/public/images/main-layer-9.jpg)', 'url(/public/images/main-layer-10.jpg)' ],
  img4: ['url(/public/images/main-layer-1.jpg)', 'url(/public/images/main-layer-5.jpg)', 'url(/public/images/main-layer-2.jpg)', 'url(/public/images/main-layer-1.jpg)' , 'url(/public/images/main-layer-8.jpg)', 'url(/public/images/main-layer-13.jpg)' ],
  img5: ['url(/public/images/main-layer-3.jpg)', 'url(/public/images/main-layer-7.jpg)', 'url(/public/images/main-layer-4.jpg)', 'url(/public/images/main-layer-2.jpg)' , 'url(/public/images/main-layer-7.jpg)', 'url(/public/images/main-layer-11.jpg)' ],
  img6: ['url(/public/images/main-layer-6.jpg)', 'url(/public/images/main-layer-9.jpg)', 'url(/public/images/main-layer-6.jpg)', 'url(/public/images/main-layer-3.jpg)' , 'url(/public/images/main-layer-6.jpg)', 'url(/public/images/main-layer-10.jpg)' ],
  img7: ['url(/public/images/main-layer-9.jpg)', 'url(/public/images/main-layer-11.jpg)', 'url(/public/images/main-layer-8.jpg)', 'url(/public/images/main-layer-4.jpg)' , 'url(/public/images/main-layer-5.jpg)', 'url(/public/images/main-layer-9.jpg)' ],
  img8: ['url(/public/images/main-layer-12.jpg)', 'url(/public/images/main-layer-13.jpg)', 'url(/public/images/main-layer-10.jpg)', 'url(/public/images/main-layer-5.jpg)' , 'url(/public/images/main-layer-4.jpg)', 'url(/public/images/main-layer-8.jpg)' ],
}




// top gallery index page

var img1_1 = document.getElementById("gallery1-1");
var img2_1 = document.getElementById("gallery1-2");
var img3_1 = document.getElementById("gallery1-3");
var img4_1 = document.getElementById("gallery1-4");
var img5_1 = document.getElementById("gallery1-5");
var img6_1 = document.getElementById("gallery1-6");
var img7_1 = document.getElementById("gallery1-7");
var img8_1 = document.getElementById("gallery1-8");


setTimeout(function(){
  console.log("timeout");
setInterval(function() {
  console.log(img1_1);
  img1_1.style.backgroundImage = images1['img1'][getRandomInt(0, 5)];
}, 3000);
}, 1000);

setTimeout(function(){
setInterval(function() {
  img2_1.style.backgroundImage = images1['img2'][getRandomInt(0, 5)];
}, 3000);
}, 2000);

setTimeout(function(){
setInterval(function() {
  img3_1.style.backgroundImage = images1['img3'][getRandomInt(0, 5)]; 
},3000);
}, 3000);

setTimeout(function(){
setInterval(function() {
  img4_1.style.backgroundImage = images1['img4'][getRandomInt(0, 5)]; 
},3000);
}, 4000);
setTimeout(function(){
  setInterval(function() {
    img5_1.style.backgroundImage = images1['img5'][getRandomInt(0, 5)];
  }, 3000);
  }, 1000);
  
  setTimeout(function(){
  setInterval(function() {
    img6_1.style.backgroundImage = images1['img6'][getRandomInt(0, 5)];
  }, 3000);
  }, 2000);
  
  setTimeout(function(){
  setInterval(function() {
    img7_1.style.backgroundImage = images1['img7'][getRandomInt(0, 5)]; 
  },3000);
  }, 3000);
  
  setTimeout(function(){
  setInterval(function() {
    img8_1.style.backgroundImage = images1['img8'][getRandomInt(0, 5)]; 
  },3000);
  }, 4000);

// bottom gallery index page

var images = {
  img1: ['url(/public/images/layer-8.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-2.jpg)','url(/public/images/layer-9.jpg)', 'url(/public/images/layer-7.jpg)', 'url(/public/images/layer-3.jpg)'],
  img2: ['url(/public/images/layer-1.jpg)', 'url(/public/images/layer-2.jpg)', 'url(/public/images/layer-3.jpg)','url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)'],
  img3: ['url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)','url(/public/images/layer-7.jpg)', 'url(/public/images/layer-8.jpg)', 'url(/public/images/layer-9.jpg)'],
  img4: ['url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)','url(/public/images/layer-7.jpg)', 'url(/public/images/layer-8.jpg)', 'url(/public/images/layer-9.jpg)'],
  img5: ['url(/public/images/layer-8.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-2.jpg)','url(/public/images/layer-9.jpg)', 'url(/public/images/layer-7.jpg)', 'url(/public/images/layer-3.jpg)'],
  img6: ['url(/public/images/layer-1.jpg)', 'url(/public/images/layer-2.jpg)', 'url(/public/images/layer-3.jpg)','url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)'],
  img7: ['url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)','url(/public/images/layer-7.jpg)', 'url(/public/images/layer-8.jpg)', 'url(/public/images/layer-9.jpg)'],
  img8: ['url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)','url(/public/images/layer-7.jpg)', 'url(/public/images/layer-8.jpg)', 'url(/public/images/layer-9.jpg)'],
}

var img1 = document.getElementById("gallery2-3");
var img2 = document.getElementById("gallery2-1");
var img3 = document.getElementById("gallery2-6");
var img4 = document.getElementById("gallery2-8");


setTimeout(function(){

setInterval(function() {
  img1.style.backgroundImage = images['img1'][getRandomInt(0, 5)];

   
}, 3000);
}, 1000);


setTimeout(function(){

setInterval(function() {

  img2.style.backgroundImage = images['img2'][getRandomInt(0, 5)];

  
}, 3000);
}, 2000);

setTimeout(function(){
setInterval(function() {

  img3.style.backgroundImage = images['img3'][getRandomInt(0, 5)];
  
 
},3000);


}, 3000);

setTimeout(function(){
setInterval(function() {

  img4.style.backgroundImage = images['img4'][getRandomInt(0, 5)];
  
 
},3000);

}, 4000);

// top mobile gallery index page

var images_mob = {
  img1: ['url(/public/images/layer-8.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-2.jpg)','url(/public/images/layer-9.jpg)', 'url(/public/images/layer-7.jpg)', 'url(/public/images/layer-3.jpg)'],
  img2: ['url(/public/images/layer-1.jpg)', 'url(/public/images/layer-2.jpg)', 'url(/public/images/layer-3.jpg)','url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)'],
  img3: ['url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)','url(/public/images/layer-7.jpg)', 'url(/public/images/layer-8.jpg)', 'url(/public/images/layer-9.jpg)'],
  img4: ['url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)','url(/public/images/layer-7.jpg)', 'url(/public/images/layer-8.jpg)', 'url(/public/images/layer-9.jpg)'],
  img5: ['url(/public/images/layer-8.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-2.jpg)','url(/public/images/layer-9.jpg)', 'url(/public/images/layer-7.jpg)', 'url(/public/images/layer-3.jpg)'],
  img6: ['url(/public/images/layer-1.jpg)', 'url(/public/images/layer-2.jpg)', 'url(/public/images/layer-3.jpg)','url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)'],
  img7: ['url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)','url(/public/images/layer-7.jpg)', 'url(/public/images/layer-8.jpg)', 'url(/public/images/layer-9.jpg)'],
  img8: ['url(/public/images/layer-4.jpg)', 'url(/public/images/layer-5.jpg)', 'url(/public/images/layer-6.jpg)','url(/public/images/layer-7.jpg)', 'url(/public/images/layer-8.jpg)', 'url(/public/images/layer-9.jpg)'],
}

var img1_2 = document.getElementById("gallery1-1-mob");
var img2_2 = document.getElementById("gallery1-2-mob");
var img3_2 = document.getElementById("gallery1-3-mob");
var img4_2 = document.getElementById("gallery1-4-mob");
var img5_2 = document.getElementById("gallery1-5-mob");
var img6_2 = document.getElementById("gallery1-6-mob");
var img7_2 = document.getElementById("gallery1-7-mob");
var img8_2 = document.getElementById("gallery1-8-mob");


setTimeout(function(){
setInterval(function() {
  img1_2.style.backgroundImage = images_mob['img1'][getRandomInt(0, 5)];
}, 3000);
}, 1000);

setTimeout(function(){
setInterval(function() {
  img2_2.style.backgroundImage = images_mob['img2'][getRandomInt(0, 5)];
}, 3000);
}, 2000);

setTimeout(function(){
setInterval(function() {
  img3_2.style.backgroundImage = images_mob['img3'][getRandomInt(0, 5)]; 
},3000);
}, 3000);

setTimeout(function(){
setInterval(function() {
  img4_2.style.backgroundImage = images_mob['img4'][getRandomInt(0, 5)]; 
},3000);
}, 4000);
setTimeout(function(){
  setInterval(function() {
    img5_2.style.backgroundImage = images_mob['img1'][getRandomInt(0, 5)];
  }, 3000);
  }, 1000);
  
  setTimeout(function(){
  setInterval(function() {
    img6_2.style.backgroundImage = images_mob['img2'][getRandomInt(0, 5)];
  }, 3000);
  }, 2000);
  
  setTimeout(function(){
  setInterval(function() {
    img7_2.style.backgroundImage = images_mob['img3'][getRandomInt(0, 5)]; 
  },3000);
  }, 3000);
  
  setTimeout(function(){
  setInterval(function() {
    img8_2.style.backgroundImage = images_mob['img4'][getRandomInt(0, 5)]; 
  },3000);
  }, 4000);

new Glide('.glide').mount()

