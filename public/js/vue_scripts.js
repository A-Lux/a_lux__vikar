Vue.component('app-some', {
    props: ['name', 'text', 'price', 'count', 'caritemsall', 'productid', 'index'],
    template:`
            <div>
                <div class="row order-item">
                    <div class="col-lg-5 col-md-12">
                        <h3>{{name}}</h3>
                        <p>{{text}}</p>
                    </div>
    
                    <div class="col-lg-2 col-12">
    
                        <div class="order-input">
                            <a class="down" id="down" href="#" @click.prevent="$emit('minus')"><img src="images/order-arrow-down.png" alt="down"></a>
                            <input type="number" min="1" :value="count" @input="onkeyup(index, productid, $event.target.value)">
                            <!--<input type="number" min="1" :value="count" @input="$emit('onkeyup', 55,$event.target.value)">-->
                            <a class="up" id="up" href="#" @click.prevent="$emit('plus')"><img src="images/order-arrow-up.png" alt="up"></a>
                        </div>
    
                    </div>
    
                    <div class="col-lg-2 col-3">
                        <h5><span class="red-text">{{price}} Тг</span></h5>
                        <p>за кв.м.</p>
                    </div>
    
                    <div class="col-lg-2 col-3">
                        <h5><span class="red-text">{{price * count}}  Тг</span></h5>
                        <p>итого</p>
                    </div>
    
                    <div class="col-lg-1 col-1">
                        <a href="#"><img src="images/order-close.png" alt="Закрыть" @click="$emit('delete')"></a>
                    </div>
                </div> 
            </div>
        `,
    methods: {
        onkeyup(index ,productid, value) {
            sample.onKeyup(index, productid, value);
        }
    }
});

var car = Vue.component('app-car', {
    props: ['count', 'summ', 'info'],
    template:`
        <div v-if="info.length">
            <h5><span class="red-text">{{summ}} Тг</span></h5> 
        </div> 
        <div v-else>Корзина пуста!</div>  
        `,
    methods:{

    },
});

var car = new Vue({
    el: ".itog",
    data: {
        parentCount: parentCountjs,
        allSumm: allSummjs,
        info: data_products,
    },
});

var sample = new Vue({
        el: ".order-content",
        data: {
            info: data_products,
        },
        methods:{
            onMinus(key){
                if(this.info[key].count!=1){
                    axios.get('/updateCart?minus=1&productId=' + this.info[key].productid)
                        .then(response => (car.parentCount = response.data.count, car.allSumm = response.data.summ, this.info[key].count--));
                }
            },
            onPlus(key){
                axios.get('/updateCart?plus=1&productId=' + this.info[key].productid)
                    .then(response => (car.parentCount = response.data.count, car.allSumm = response.data.summ, this.info[key].count++));
            },
            onKeyup(index, key, value){
                axios.get('/updateCart?keyup=1&productId=' + key + '&value=' + value)
                    .then(response => (car.parentCount = response.data.count, car.allSumm = response.data.summ, this.info[index].count = response.data.count));
            },
            onDelete(key){
                axios.get('/delete?productId=' + this.info[key].productid)
                    .then(response => (car.parentCount = response.data.count, car.allSumm = response.data.summ, this.info.splice(key, 1)));
            },
        },
        computed:{
            summ(){
                return this.price * this.count;
            }
        },
});

