<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordering extends Model
{
    public function orders()
    {
        return $this->hasMany(Order::class, 'ordering');
    }
}
