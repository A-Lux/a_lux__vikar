<?php

namespace App\Http\Controllers\Voyager;

use App\Calc;
use App\Catalog;
use App\ProductsCalctwo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;

class CatalogsController extends Controller
{
    public $redirectTo = '/admin/catalogs';

    public function update(Request $request, int $id): RedirectResponse
    {
        $files = $this->uploadFiles($request, $id);

        $delete = 0;
        if($request->calc) {
            //ProductsCalctwo::where('product_id', $id)->delete();
            foreach ($request->calc as $k1 => $val) {
                foreach($val as $k2 => $val1) {
                    if(is_array($val1)) {
                        foreach ($val1 as $k => $v) {
                            $calc = ProductsCalctwo::find($k);
                            $calc->product_id = $id;
                            $calc->calc_id = $k2;
                            $calc->price = $v;
                            if (isset($files[$k2]))
                                $calc->img = $files[$k2];
                            $calc->save();
                        }
                    }else{
                        if($delete == 0)
                            ProductsCalctwo::where('product_id', $id)->delete();
                        $delete = 1;
                        $calc = new ProductsCalctwo;
                        $calc->product_id = $id;
                        $calc->calc_id = $k2;
                        $calc->price = $val1;
                        if (isset($files[$k2]))
                            $calc->img = $files[$k2];
                        $calc->save();
                    }
                }
            }
        }

		if($request->img){
				foreach($request->img as $v){
					$time = time();
					$files[] = '/catalogs/'.$id.'/'.$time.$v->hashName();

					Storage::put(
						'/public/catalogs/'.$id.'/'.$time.$v->hashName(),
						file_get_contents($v->getRealPath())
					);
				}

			$img = json_encode($files);

			Catalog::find($id)->update(array_merge($request->all(), [
				'img' => $img,
			]));
		}else
        	Catalog::find($id)->update($request->all());

        \Artisan::call('cache:clear');

        return redirect()->to(url($this->redirectTo));
    }

    public function store(Request $request): RedirectResponse
    {	
		if(!$request->ajax()){
			$model = Catalog::create(array_merge($request->all(), [
					'img' => '',
			]));

			if($request->img){
					foreach($request->img as $v){
						$time = time();
						$files[] = '/catalogs/'.$model->id.'/'.$time.$v->hashName();

						Storage::put(
							'/public/catalogs/'.$model->id.'/'.$time.$v->hashName(),
							file_get_contents($v->getRealPath())
						);
					}

				$img = json_encode($files);

				Catalog::find($model->id)->update(array_merge($request->all(), [
					'img' => $img,
				]));
			}

			$files = $this->uploadFiles($request, $model->id);

			if($request->calc) {
				foreach ($request->calc as $k1 => $val) {
					foreach($val as $k => $v) {
						$calc = new ProductsCalctwo;
						$calc->product_id = $model->id;
						$calc->calc_id = $k;
						$calc->price = $v;
						if(isset($files[$k]))
							$calc->img = $files[$k];
						$calc->save();
					}
				}
			}
			\Artisan::call('cache:clear');

			return redirect()->to(url($this->redirectTo));
		}
    }

    protected function uploadFiles($request, $id): array
    {
        $files = [];

        if($request->images)
        foreach ($request->images as $k1 => $val) {
            foreach($val as $k => $v) {
                $time = time();
                $files[$k] = $time.$v->hashName();

                Storage::put(
                    '/public/productpriceimg/'.$id.'/'.$time.$v->hashName(),
                    file_get_contents($v->getRealPath())
                );
            }
        }

        return $files;
    }

    public function category()
    {
        if($_GET['category_id'] == 17 || $_GET['category_id'] == 18) {
            $model = '';
            if($_GET['product_id'])
                $model = Catalog::find($_GET['product_id']);

            $category_id = $_GET['category_id'];
            $product_id  = $_GET['product_id'];
            $calcs = Calc::where('category_id', $category_id)->get();
            return view('vendor.voyager.catalogs.category', compact('model','calcs', 'category_id', 'product_id'));
        }else
            echo '';
    }
}
