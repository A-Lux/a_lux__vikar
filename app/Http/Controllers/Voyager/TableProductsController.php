<?php

namespace App\Http\Controllers\Voyager;

use App\FilterTovar;
use App\Tovary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManager;
use TCG\Voyager\Facades\Voyager;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

class TableProductsController extends Controller
{
    public function update(Request $request, int $id): RedirectResponse
    {
        die;
        Tovary::find($id)->update(array_merge([
            'relations' => $relations,
            'img'       => $filename,
            'images'    => $images
        ], $request->all()));

        \Artisan::call('cache:clear');

        return redirect()->to(url($this->redirectTo));
    }
}
