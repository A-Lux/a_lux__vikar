<?php

namespace App\Http\Controllers;

use App\CategoryTovary;
use App\MenuSite;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public $menu;
    public $menufooter;
    public $categoryes;

    public function __construct()
    {
        $this->menu = $this->getMenu();
    }

    public function getMenu(){
        return MenuSite::where('status', 1)
                       ->where('position', 1)
                       ->orderBy('sort', 'ASC')
                       ->get();
    }

    public static function cutStr($str, $length=50, $postfix='...')
    {
        if ( strlen($str) <= $length)
            return $str;

        $temp = substr($str, 0, $length);
        return substr($temp, 0, strrpos($temp, ' ') ) . $postfix;
    }
}
