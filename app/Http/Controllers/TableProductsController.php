<?php

namespace App\Http\Controllers;

use App\Calcred;
use App\Tableproduct;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class TableProductsController extends Controller
{
    protected $redirectTo = '/admin/tableproducts';

    public function update(Request $request, int $id): RedirectResponse
    {
        $document = \phpQuery::newDocumentHTML($request->table);

        $tr_first = $document->find('table');

        foreach ($tr_first as $tr) {
            $tr = pq($tr);

            $td = $tr->find('tr:first td');

            Calcred::where('category_id', $id)->delete();
//            $parent_id_arr = [];
            foreach ($td as $td_child) {
                $td_child = pq($td_child);
                $calcred = new Calcred();
                $calcred->name = $td_child->html();
                $calcred->category_id = $id;
                $calcred->parent_id = 0;
                $calcred->save();
//                $parent_id_arr[] = $calcred->id;
            }
            $td_count = count($tr->find('tr'));

            for($i = 2;$i <= $td_count;$i++) {
                $td = $tr->find('tr:nth-child('.$i.') td');

                $parent_id = 0;
                foreach ($td as $k => $td_child) {
                    $td_child = pq($td_child);
                    $calcred = new Calcred();
                    $calcred->name = $td_child->html();
                    $calcred->category_id = $id;
                    $calcred->parent_id = $parent_id;
                    $calcred->save();
                    if ($k == 0)
                        $parent_id = $calcred->id;
                }
            }
        }

        Tableproduct::find($id)->update($request->all());

        \Artisan::call('cache:clear');

        return redirect()->to(url($this->redirectTo));
    }
}
