<?php

namespace App\Http\Controllers;

use App\Catalog;
use App\Contact;
use App\Delivery;
use App\Job;
use App\Map;
use App\MenuSite;
use App\PhotoVideo;
use App\TovaryIUslusi;

class SiteController extends FrontendController
{
    public function index()
    {
        $model = MenuSite::where('url', '/')->first();
        $menu = $this->menu;
        $menu2 = MenuSite::where('status', 1)->where('position', 2)->orderBy('sort', 'ASC')->get();
        return view('site.index', compact('model', 'menu', 'menu2'));
    }

    public function menu($url)
    {
        $menu = $this->menu;
        $model = MenuSite::where('url', $url)->first();

        if($url=='goods-and-services'){
            $uslugi = TovaryIUslusi::where('category', 1)->get();
            return view('site.goods-and-services', compact('model', 'menu', 'uslugi'));
        }

        if($url=='calc'){
            $uslugi = TovaryIUslusi::where('category', 2)->get();
            return view('site.goods-and-services', compact('model', 'menu', 'uslugi'));
        }

        if($url=='contact-details'){
            $model = Contact::first();
            return view('site.contacts', compact('model', 'menu', 'uslugi'));
        }

        if($url=='photo-video-report'){
            $photo_video_big   = PhotoVideo::where('position', 1)->limit(3)->get();
            $photo_video_small = PhotoVideo::where('position', 2)->limit(12)->get()->toArray();
            return view('site.photo-video-report', compact('model', 'menu', 'photo_video_big', 'photo_video_small'));
        }

        if($url=='jobs'){
            $jobs = Job::get();
            return view('site.jobs', compact('model', 'menu', 'jobs'));
        }

        if($url=='shipping-and-payment'){
            $deliveries = Delivery::orderBy('radius', 'ASC')->get();
            $map = Map::first();
            return view('site.polyline', compact('model', 'menu', 'deliveries', 'map'));
        }
    }

    public function product($url)
    {
        $menu = $this->menu;
        $model = TovaryIUslusi::where('url', $url)->first();
        $catalogs = Catalog::where('category_id', $model->id)->get();

        return view('site.catalogs', compact('model', 'menu', 'catalogs', 'url'));
    }

    public function tovar($url, $tovar)
    {
        $menu = $this->menu;
        $model = Catalog::where('url', $tovar)->first();
        $jquery = 1;

        if($model->menu->category == 1)
            return view('site.tovar', compact('model', 'menu', 'jquery', 'url', 'tovar'));
        else{
            if($model->category_id == 16)
                return view('site.tovar16', compact('model', 'menu', 'jquery', 'url', 'tovar'));
            if($model->category_id == 17)
                return view('site.tovar17', compact('model', 'menu', 'jquery', 'url', 'tovar'));
            if($model->category_id == 18)
                return view('site.tovar18', compact('model', 'menu', 'jquery', 'url', 'tovar'));
        }
    }

    public function tovarorder($url, $tovar)
    {
        $menu = $this->menu;
        $model = Catalog::where('url', $tovar)->first();
        $jquery = 1;

        return view('site.tovarorder', compact('model', 'menu', 'jquery', 'url', 'tovar'));
    }

    public function polyline()
    {
        return view('site.polyline');
    }
}
