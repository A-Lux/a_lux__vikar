<?php

namespace App\Http\Controllers;

use App\Calcred;
use App\Catalog;
use App\Order;
use App\Ordering;
use App\TovaryIUslusi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Paybox\Pay\Facade as PayboxApi;

class OrderController extends FrontendController
{
    public function order()
    {
//        dd(Session::all());
//        foreach(Session::get('cart') as $k => $v){
//            $product = Tovary::find($k);
//            dd($product->price);
//        }
        $menu = $this->menu;

        return view('order.index', compact('menu'));
    }

    public function addToCart()
    {
        $productId = $_GET['productId'];

//        Session::flush();
        if (Session::has("cart.$productId.0"))
            Session::increment("cart.$productId.0", 1);
        else{
            Session::push("cart.$productId", 1);

            $product = Catalog::find($productId);
            Session::push("cart.$productId", $product->price50);
        }

//        dd(Session::get('cart'));
//        foreach(Session::get('cart') as $k => $v){
//            echo $v;die;
//        }

        Session::push("cartcount", 1);
    }

    public function updateCart()
    {
//        Session::flush();
        $productId = $_GET['productId'];

        if(isset($_GET['minus']))
            Session::increment("cart.$productId.0", -1);
        if(isset($_GET['plus']))
            Session::increment("cart.$productId.0", 1);
        if(isset($_GET['keyup'])){
            Session::forget("cart.$productId");
            Session::push("cart.$productId", (int)$_GET['value']);

            $product = Catalog::find($productId);
            Session::push("cart.$productId", $product->price50);
        }

        Session::forget("cartcount");

        $count = 0;
        $summ  = 0;
        foreach(Session::get('cart') as $k => $v){
            $count += $v[0];
            $summ  += $v[0] * $v[1];
        }

        Session::push("cartcount", $count);

        $array = ['count' => $count, 'summ' => $summ];
        return json_encode($array);
    }

    public function delete()
    {
        $productId = $_GET['productId'];

        Session::forget("cart.$productId");

        Session::forget("cartcount");

        $count = 0;
        $summ  = 0;
        foreach(Session::get('cart') as $k => $v){
            $count += $v[0];
            $summ  += $v[0] * $v[1];
        }

        Session::push("cartcount", $count);

        $array = ['count' => $count, 'summ' => $summ];
        return json_encode($array);
    }

    public function oformit_zacaz2(Request $request)
    {
        $model = Catalog::find($request->id);
        $array_ar = [];
        if($model->category_id == 16){
            $type = array_search($request->type, $model->type1);
            $where = array_search($request->where, $model->where1);
            if($type === false || $where === false) {
                echo 'Что то пошло не так, пожалуйста попробуйте позже!';
                die;
            }
            $price = 0;
            foreach($model->calcs as $calc){
                if($calc->calc_id == 1)
                    $price += $request->quantity_sm * $calc->price;
                if($calc->calc_id == 2)
                    $price += $request->quantity_col * $calc->price;
                if($request->type == $calc->calc_id || $request->where == $calc->calc_id) {
                    $price += $calc->price;
                    $array_ar[] = $calc->calc_id;
                }
            }
        }
        if($model->category_id == 17){
            $type = array_search($request->type, $model->type2);
            $yarkost = array_search($request->yarkost, $model->yarkost);
            $dop = array_search($request->dop, $model->dop);
            if($type === false || $yarkost === false || $dop === false) {
                echo 'Что то пошло не так, пожалуйста попробуйте позже!';
                die;
            }
            $price = 0;
            foreach($model->calcs as $calc){
                if($calc->calc_id == 7)
                    $price += $request->quantity_sm * $calc->price;
                if($calc->calc_id == 8)
                    $price += $request->quantity_col * $calc->price;
                if($request->type == $calc->calc_id || $request->yarkost == $calc->calc_id || $request->dop == $calc->calc_id) {
                    $price += $calc->price;
                    $array_ar[] = $calc->calc_id;
                }
            }
        }
        if($model->category_id == 18){
            $price = 0;
            foreach($model->calcs as $calc){
                if($calc->calc_id == 18)
                    $price += $request->quantity_sm * $calc->price;
                if($calc->calc_id == 19)
                    $price += $request->quantity_col * $calc->price;
            }
        }

        $id = $model->id;
        $array = [];
        $array['array'] = $array_ar;
        $array['quantity_sm'] = $request->quantity_sm;
        $array['quantity_col'] = $request->quantity_col;
        $array['type'] = 2;
        $array['price'] = $price;
        Session::forget("cart");
        if (Session::has("cart.$id")) {
            Session::push("cart.$id", $array);
        }
        else{
            Session::push("cart.$id", $array);
        }

        return redirect()->to('/pay');
    }

    public function tableproducts()
    {
        $error = 1;
        foreach(Session::get("cart") as $k => $v) {
            $tableproducts = Catalog::find($k);
            $calcreds = Calcred::where('category_id', $tableproducts->category_id)->get();

            foreach($calcreds as $v1){
                if($v[0][1] == $v1->name)
                    $error = 0;
            }
        }
        if($error)
            echo 'Что то пошло не так, пожалуйста попробуйте позже!';

        return redirect()->to('/pay');
    }
	
    public function pay()
    {
        $menu = $this->menu;

        return view('cart.pay', compact('menu', 'categoryes', 'menufooter'));
    }

    public function orderitog()
    {
        $productId = $_GET['productId'];
        $array = [$_GET['col_order'], $_GET['cena_ob'], $_GET['u_dizainera'], $_GET['dostavka'], $_GET['adres'], 'type' => 1];//количество, цена, усл. дизайнера,цена доставки, адрес
        Session::forget("cart");
        if (Session::has("cart.$productId")) {
            Session::push("cart.$productId", $array);
        }
        else{
            Session::push("cart.$productId", $array);
        }
        return json_encode(Session::get("cart.$productId"));
    }

    public function payend(Request $request)
    {
        $this->validate($request, [
            'fio' => 'required',
            'phone' => 'required',
            'adres' => 'required',
            'email' => 'required|email'
        ]);

        $ordering = new Ordering();

        $ordering->fio   = $request->fio;
        $ordering->phone = $request->phone;
        $ordering->adres = $request->adres;
        $ordering->email = $request->email;

        if(\Auth::user()){
            $ordering->user_id = \Auth::user()->id;
        }

        if($ordering->save()){
            foreach(Session::get("cart") as $k => $v) {
                //$product = TovaryIUslusi::find($k);

                $order = new Order();
                $order->item_id  = $k;
                $order->type     = $v[0]['type'];
                if($v[0]['type'] == 1) {
                    $order->price = $v[0][1];
                    $order->quantity = $v[0][0];
                    $order->disainer = $v[0][2];
                    $order->delivery = $v[0][3];
                    $order->adres_delivery = $v[0][4];
                }else{
                    $order->price = $v[0]['price'];
                    $order->quantity_sm = $v[0]['quantity_sm'];
                    $order->quantity_col = $v[0]['quantity_col'];
                    $array_calc = "[";
                    foreach ($v[0]['array'] as $v){
                        $array_calc .= "$v,";
                    }
                    $array_calc = substr($array_calc, 0, -1);
                    $array_calc .= "]";
                    $order->array_calc = $array_calc;
                }
                if(\Auth::user()){
                    $order->user_id = \Auth::user()->id;
                }
                $order->ordering = $ordering->id;
                $order->status   = 0;
                if($order->save())
                    $this->paybox($order->id, "Оплата товара 111", 5555, $ordering->phone, $ordering->email);
                die;

            }
        }

        Session::forget("cart");
        Session::forget("cartcount");

        return redirect()->back();
    }

    public function paybox($order_id, $description, $price, $phone, $email)
    {
        $paybox = new PayboxApi();

        $paybox->merchant->id = env('PAYBOX_ID');
        $paybox->merchant->secretKey = env('PAYBOX_SECRET_KEY');
        $paybox->order->id = $order_id;
        $paybox->order->description = $description;
        $paybox->order->amount = $price;
        $paybox->config->userPhone = $phone;
        $paybox->config->userContactEmail = $email;
//        $paybox->order->testing_mode = 1;
//        $paybox->config->checkUrl   = "http://kupipolis.ibeacon.kz/site/check";
        $paybox->config->resultUrl  = env('PAYBOX_URL')."/order/result";
        $paybox->config->successUrl = env('PAYBOX_URL')."/order/success";
        $paybox->config->failureUrl = env('PAYBOX_URL')."/order/failure";

        $paybox->config->requestMethod    = "GET";
        $paybox->config->successUrlMethod = "GET";
        $paybox->config->failureUrlMethod = "GET";
//        pg_testing_mode=1
        if($paybox->init()) {
            header('Location:' . $paybox->redirectUrl);
            die;
        }
    }

    public function result()
    {
        if($_GET['pg_can_reject'] == 1){
            $order = Order::find($_GET['pg_order_id']);
            $order->status = 1;
            $order->save();
        }else{
            $paybox = new PayboxApi();
            echo $paybox->cancel('Ошибка');
        }
        die;
    }

    public function success()
    {
        echo "Оплата прошла успешно";
    }

    public function failure()
    {
        echo "Платеж отменен";
        die;
    }
}
