<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsCalctwo extends Model
{
    protected $fillable = [
        'calc_id',
        'product_id',
        'price',
		'img',
        'created_at',
        'updated_at',
    ];
}
