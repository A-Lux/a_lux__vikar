<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $fillable = [
        'category_id',
        'name',
        'text_opisani',
        'price',
        'text',
        'title',
        'description',
        'sort',
        'url',
        'img',
        'created_at',
        'updated_at',
    ];

    public $type1 = [
        3,4
    ];

    public $where1 = [
        5,6
    ];

    public $type2 = [
        9, 10, 11, 12, 13
    ];

    public $yarkost = [
        14, 15
    ];

    public $dop = [
        16, 17
    ];

    public function tableproducts()
    {
        return $this->hasMany(Tableproduct::class, 'catalog_id');
    }

    public function menu()
    {
        return $this->belongsTo(TovaryIUslusi::class, 'category_id');
    }

    public function calcs()
    {
        return $this->hasMany(ProductsCalctwo::class, 'product_id');
    }
}
