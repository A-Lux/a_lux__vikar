<?php

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::resource('tableproducts', 'TableProductsController', ['only' => ['update']]);
    Route::resource('catalogs', '\App\Http\Controllers\Voyager\CatalogsController', ['only' => ['update', 'store']]);
    Route::get('/category_tovary', '\App\Http\Controllers\Voyager\CatalogsController@category');
});

Route::get('/pay', 'OrderController@pay');
Route::get('/oformit_zacaz2', 'OrderController@oformit_zacaz2');
Route::get('/tableproducts', 'OrderController@tableproducts');
Route::post('/payend', 'OrderController@payend');
Route::get('/order/result', 'OrderController@result');
Route::get('/order/success', 'OrderController@success');
Route::get('/order/failure', 'OrderController@failure');
Route::get('/order/orderitog', 'OrderController@orderitog');
Route::get('/updateCart', 'OrderController@updateCart');
Route::get('/delete', 'OrderController@delete');
Route::get('/order', 'OrderController@order');
Route::get('/order/addToCart', 'OrderController@addToCart');

Route::get('/', 'SiteController@index')->name('index');
Route::get('polyline', 'SiteController@polyline');
Route::get('{url}', 'SiteController@menu');
Route::get('product/{url}', 'SiteController@product');
Route::get('product/{url}/{tovar}', 'SiteController@tovar');
Route::get('product/{url}/{tovar}/order', 'SiteController@tovarorder');//order

