@extends('layouts.main')

@section('content')
    <main class="main">
        <div class="container">
            <h3 class="pay-sum">Итого 5555 тенге.</h3>
            <form method="POST" action="/payend">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{ csrf_field() }}
                <div class="pay-form-control">
                <label>ФИО</label><input type="text" name="fio" value="@if(old('fio')) {{ old('fio') }} @endif">
                </div>
                <div class="pay-form-control">
                <label>Телефон</label><input type="text" name="phone" value="@if(old('phone')) {{ old('phone') }} @endif">
                </div>
                <div class="pay-form-control">
                <label>Адрес</label><input type="text" name="adres" value="@if(old('adres')) {{ old('adres') }} @endif">
                </div>
                <div class="pay-form-control">
                <label>E-mail</label><input type="text" name="email" value="@if(old('email')) {{ old('email') }} @endif">
                </div>
                <button class="buy-btn mb-5 mt-2">Оплатить</button>
            </form>
        </div>
    </main>
@endsection
