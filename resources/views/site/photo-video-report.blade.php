@extends('layouts.main')

@section('content')

    <!-- VIKAR GALLERY -->
    <div class="container">
        <div class="vikar-gallery">
            <div class="row">
                <div class="col gallery-title">
                    <h1>Фото/видео <span class="gallery-title-red-text">отчет</span></h1>
                </div>
            </div>

            <!-- 1 СТРОКА -->
			<div class="row gallery-content">
                                                            <div class="col-lg-6 col-12 gallery-lg-img">
                                                            <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/zywStEN2DRT4g67x2D26.jpg">
                                    <img  class="img-fluid" src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/zywStEN2DRT4g67x2D26.jpg" alt="Видео" style="    padding-bottom: 1rem;">
                                </a>
                                                    </div>
                        <div class="col-lg-6 col-12 gallery-sm-img">
                          <div class="row">
								<div class="col-sm-6">
								  <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/rOzQ8StXIigbdrSfJv1h.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/rOzQ8StXIigbdrSfJv1h.jpg"  class="img-fluid" alt="Фото"></a>
								</div>
							  <div class="col-sm-6">
								  <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/ZAMkTiPHLl6xF0vTNPsA.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/ZAMkTiPHLl6xF0vTNPsA.jpg" class="img-fluid" alt="Фото"></a>
								</div>
							  <div class="col-sm-6">
								 <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/yg51mH2Gxtk3C5gPyk7i.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/yg51mH2Gxtk3C5gPyk7i.jpg" class="img-fluid" alt="Фото"></a>
								</div>
							  <div class="col-sm-6">
								  <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/rUAOgPYxuEPFKT1T1OpL.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/rUAOgPYxuEPFKT1T1OpL.jpg" class="img-fluid" alt="Фото"></a>
								</div>
							</div>                                                                    
                           </div>
				<div class="col-lg-6 col-12 gallery-sm-img">
                          <div class="row">
								<div class="col-sm-6">
									<!-- Button trigger modal -->
									<a type="button"  data-toggle="modal" data-target="#video1" class="btn-play-gallery-parent">
										<img src="/public/images/vid1.jpg">
										<img src="/public/images/play-btn.png" class="btn-play-gallery">
                                    </a>

									<!-- Modal -->
									<div class="modal fade" id="video1" tabindex="-1" role="dialog" aria-labelledby="video1" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
												</div>
												<div class="modal-body">
													<iframe width="100%" height="100%" src="https://www.youtube.com/embed/RJCLnFOfFKQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							  <div class="col-sm-6">
								  <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/ZAMkTiPHLl6xF0vTNPsA.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/ZAMkTiPHLl6xF0vTNPsA.jpg" class="img-fluid" alt="Фото"></a>
								</div>
							  <div class="col-sm-6">
								 <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/yg51mH2Gxtk3C5gPyk7i.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/yg51mH2Gxtk3C5gPyk7i.jpg" class="img-fluid" alt="Фото"></a>
								</div>
							  <div class="col-sm-6">
								  	<!-- Button trigger modal -->
									<a type="button" data-toggle="modal" data-target="#video2" class="btn-play-gallery-parent">
										<img src="/public/images/vid1.jpg">
										<img src="/public/images/play-btn.png" class="btn-play-gallery">
                                    </a>

									<!-- Modal -->
									<div class="modal fade" id="video2" tabindex="-1" role="dialog" aria-labelledby="video2" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
												</div>
												<div class="modal-body">
													<iframe width="100%" height="100%" src="https://www.youtube.com/embed/RJCLnFOfFKQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>                                                                    
                           </div>                                                                               
                        <div class="col-lg-6 col-12 gallery-lg-img">
                                                        	<!-- Button trigger modal -->
									<a type="button"  data-toggle="modal" data-target="#video3" class="btn-play-gallery-parent">
										<img src="/public/images/vid1.jpg">
										<img src="/public/images/play-btn.png" class="btn-play-gallery">
                                    </a>

									<!-- Modal -->
									<div class="modal fade" id="video3" tabindex="-1" role="dialog" aria-labelledby="video3" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
												</div>
												<div class="modal-body">
													<iframe width="100%" height="100%" src="https://www.youtube.com/embed/RJCLnFOfFKQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
                                                    </div>
                                                </div>
			<div class="row gallery-content gallery-content-hidden">
                                                            <div class="col-lg-6 col-12 gallery-lg-img">
                                                            <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/zywStEN2DRT4g67x2D26.jpg">
                                    <img  class="img-fluid" src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/zywStEN2DRT4g67x2D26.jpg" alt="Видео" style="    padding-bottom: 1rem;">
                                </a>
                                                    </div>
                        <div class="col-lg-6 col-12 gallery-sm-img">
                          <div class="row">
								<div class="col-sm-6">
								  <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/rOzQ8StXIigbdrSfJv1h.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/rOzQ8StXIigbdrSfJv1h.jpg"  class="img-fluid" alt="Фото"></a>
								</div>
							  <div class="col-sm-6">
								  <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/ZAMkTiPHLl6xF0vTNPsA.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/ZAMkTiPHLl6xF0vTNPsA.jpg" class="img-fluid" alt="Фото"></a>
								</div>
							  <div class="col-sm-6">
								 <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/yg51mH2Gxtk3C5gPyk7i.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/yg51mH2Gxtk3C5gPyk7i.jpg" class="img-fluid" alt="Фото"></a>
								</div>
							  <div class="col-sm-6">
								  <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/rUAOgPYxuEPFKT1T1OpL.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/rUAOgPYxuEPFKT1T1OpL.jpg" class="img-fluid" alt="Фото"></a>
								</div>
							</div>                                                                    
                           </div>
				<div class="col-lg-6 col-12 gallery-sm-img">
                          <div class="row">
								<div class="col-sm-6">
									<!-- Button trigger modal -->
									<a type="button"  data-toggle="modal" data-target="#video1" class="btn-play-gallery-parent">
										<img src="/public/images/vid1.jpg">
										<img src="/public/images/play-btn.png" class="btn-play-gallery">
                                    </a>

									<!-- Modal -->
									<div class="modal fade" id="video1" tabindex="-1" role="dialog" aria-labelledby="video1" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
												</div>
												<div class="modal-body">
													<iframe width="100%" height="100%" src="https://www.youtube.com/embed/RJCLnFOfFKQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							  <div class="col-sm-6">
								  <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/ZAMkTiPHLl6xF0vTNPsA.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/ZAMkTiPHLl6xF0vTNPsA.jpg" class="img-fluid" alt="Фото"></a>
								</div>
							  <div class="col-sm-6">
								 <a href="http://vikar.ibeacon.kz/storage/photo-videos/January2019/yg51mH2Gxtk3C5gPyk7i.jpg"><img src="http://vikar.ibeacon.kz/storage/photo-videos/January2019/yg51mH2Gxtk3C5gPyk7i.jpg" class="img-fluid" alt="Фото"></a>
								</div>
							  <div class="col-sm-6">
								  	<!-- Button trigger modal -->
									<a type="button" data-toggle="modal" data-target="#video2" class="btn-play-gallery-parent">
										<img src="/public/images/vid1.jpg">
										<img src="/public/images/play-btn.png" class="btn-play-gallery">
                                    </a>

									<!-- Modal -->
									<div class="modal fade" id="video2" tabindex="-1" role="dialog" aria-labelledby="video2" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
												</div>
												<div class="modal-body">
													<iframe width="100%" height="100%" src="https://www.youtube.com/embed/RJCLnFOfFKQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>                                                                    
                           </div>                                                                               
                        <div class="col-lg-6 col-12 gallery-lg-img">
                                                        	<!-- Button trigger modal -->
									<a type="button"  data-toggle="modal" data-target="#video3" class="btn-play-gallery-parent">
										<img src="/public/images/vid1.jpg">
										<img src="/public/images/play-btn.png" class="btn-play-gallery">
                                    </a>

									<!-- Modal -->
									<div class="modal fade" id="video3" tabindex="-1" role="dialog" aria-labelledby="video3" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
												</div>
												<div class="modal-body">
													<iframe width="100%" height="100%" src="https://www.youtube.com/embed/RJCLnFOfFKQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
                                                    </div>
                                                </div>
			
           <!-- <div class="row gallery-content">
                @foreach($photo_video_big as $k => $v)
                    @if($k == 0 || $k % 2 == 2)
                        <div class="col-lg-6 col-12 gallery-left{{$k+1}}">
                            @if($v->type==1)
                                <a href="{{Voyager::image($v->img)}}">
                                    <img src="{{Voyager::image($v->img)}}" alt="Видео">
                                </a>
                            @else
                                <iframe width="100%" height="400" src="{{$v->video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            @endif
                        </div>
                        <div class="col-lg-6 col-12 gallery-right{{$k+1}}">
                            @foreach($photo_video_small as $k => $v1)
                                @if($v1['type']==1)
                                    <a href="{{Voyager::image($v1['img'])}}"><img src="{{Voyager::image($v1['img'])}}" alt="Фото"></a>
                                @else
                                    <iframe width="50%" height="200" src="{{$v1['video']}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                @endif
                                @php
                                    unset($photo_video_small[$k]);
                                @endphp
                                @if($k==3)
                                    @break
                                @endif
                            @endforeach
                        </div>
                    @else
                        <div class="col-lg-6 col-12 gallery-right{{$k+1}}">
                            @foreach($photo_video_small as $k => $v1)
                                @if($v1['type']==1)
                                    <a href="{{Voyager::image($v1['img'])}}"><img src="{{Voyager::image($v1['img'])}}" alt="Фото"></a>
                                @else
                                    <iframe width="50%" height="200" src="{{$v1['video']}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                @endif
                                @php
                                    unset($photo_video_small[$k]);
                                @endphp
                                @if($k==3)
                                    @break
                                @endif
                            @endforeach
                        </div>
                        <div class="col-lg-6 col-12 gallery-left{{$k+1}}">
                            @if($v->type==1)
                                <a href="{{Voyager::image($v->img)}}">
                                    <img src="{{Voyager::image($v->img)}}" alt="Видео">
                                </a>
                            @else
                                <iframe width="100%" height="400" src="{{$v->video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            @endif
                        </div>
                    @endif
                @endforeach
            </div>-->
            <!--END 1 СТРОКА -->

            <div class="row">
                <div class="col-12">
                    <div class="gallery-btn">
                        <a >Еще работы</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END VIKAR GALLERY -->

@endsection