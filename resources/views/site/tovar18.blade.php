@extends('layouts.main')

@section('content')
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <!--MAIN-->
    <main class="main">
        <div class="container">
            <div class="main-left-title">
                <h3>{{$model->name}}</h3>
            </div>

            <div class="main-content">
                <div class="row">
                    <div class="col-lg-6">

                        <div id="calc-block" class="calc-img fotorama" data-nav="thumbs" data-arrows="true">
                            @php
                                $images = json_decode($model->img);
                            @endphp
                            @if(count($images))
                                @foreach($images as $v)
                                    <a href="/storage/{{$v}}"><img src="/storage/{{$v}}" class="img-fluid" alt="img"></a>
                                    <img src="" class="img-fluid" alt="">sss
                                @endforeach
                            @endif
                        </div>

                        {!! $model->text !!}
                    </div>

                    <div class="col-lg-6 wow fadeInRightBig">
                        <form action="/oformit_zacaz2">
                            <div class="calc-bg">

                                <div class="calc-dark-text">
                                    Световой короб "Прямой"
                                </div>
                                <div class="number-input operations-block">
                                    <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation" >-</span>
                                    <input class="quantity text-center form-control input-text quantity_sm" min="0" name="quantity_sm" value="50" type="number">
                                    <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus btn-operation">+</span>
                                </div>
                                <div class="scale-text">
                                    Высота, см
                                </div>
                                <div class="number-input operations-block">
                                    <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation" >-</span>
                                    <input class="quantity text-center form-control input-text quantity_col" min="0" name="quantity_col" value="50" type="number">
                                    <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus btn-operation">+</span>
                                </div>
                                <div class="scale-text sm">
                                    Количество
                                </div>
                                <input type="hidden" value="{{$model->id}}" name="id">
                                <input type="hidden" value="{{$model->calcs[0]->price}}" name="price_sm">
                                <input type="hidden" value="{{$model->calcs[1]->price}}" name="price_col">
                                <div class="label-text">
                                    Добавить комментарий
                                </div>
                                <textarea name="" id="" rows="3" class="form-control"></textarea>
                            </div>
                            <div class="button-block">
                                <a href="" class="btn price-button">Ваша цена: <span>
                                        {{$model->calcs[0]->price * 50 + $model->calcs[1]->price * 50}} тг.
                                    </span></a>
                                <a href="" class="btn send-button oformit_zacaz2">Оформить заказ</a>
                            </div>

                        </form>
                    </div>

                </div>

                @if($model->tableproducts)
                    @foreach($model->tableproducts as $tableproducts)
                        <div class="card-table wow slideInUp inc0" data-wow-duration="1.5s">
                            {!! $tableproducts->table !!}
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </main>
    <!--END MAIN-->
    </div>

@endsection
