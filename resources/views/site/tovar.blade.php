@extends('layouts.main')

@section('content')
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <!--MAIN-->	
    <main class="main">
		<div class="overlay">			
			<div class="modal-ty">
				<div class="close-modal">
					 &#10005
				</div>
				<h3>Спасибо!</h3>
                <h3>Ваша заявка принята!</h3>
			</div>
	</div>
        <div class="container">
            <div class="main-left-title">
                <h3>{{$model->name}}</h3>
            </div>

            <div class="main-content">
                <div class="products-card">
                    <div class="col-lg-6 card-item wow slideInLeft" data-wow-duration="1.5s">
                        <div class="fotorama">
                            @php
                                $images = json_decode($model->img);
                            @endphp
                                @if(count($images))
                                    @foreach($images as $v)
                                        <img src="/storage/{{$v}}" alt="">sss
                                    @endforeach
                                @endif
                        </div>
                    </div>

                    <div class="col-lg-6 product-buy wow slideInRight" data-wow-duration="1.5s">
                        <div class="product-price-block">
                            <div class="price-block">
                                <p>{{$model->price}} Тг./кв.м.</p>
                                <span>Минимальный заказ 3 кв.м.</span>
                            </div>
                            <div class="buy-block">
                                <button class="buy-btn" onclick="window.location.href = '/product/{{$url}}/{{$tovar}}/order'">Купить</button>
                            </div>
                        </div>
                        <!--END PRICE AND BUY-->

                        <div class="products-card-tel-nums">
                            <p><a href="tel: +7 (727) 393-35-79">+7 (727) 393-35-79</a></p>
                            <p><a href="tel: +7 (727) 393-82-75">+7 (727) 393-82-75</a></p>
                        </div>
                        <!--END CARD'S NUMBERS-->

                        <div class="products-card-search-block">
                            <form action="#">
                                <div class="input-tel">
                                    <input class="phone_us" type="tel" placeholder="Введите Ваш номер и мы перезвоним">
                                </div>
                                <button class="btn-send">отправить</button>
                            </form>
                        </div>						
                        <!--END INPUT NUMBER-->

                        <div class="conditions-payment">
                            <h4>Условия оплаты:</h4>
                            <ul>
                                <li>Оплата через платежную систему QIWI</li>
                                <li>Наличными Курьеру</li>
                                <li>Банковский перевод на расчетный счет</li>
                                <li>Онлайн-оплата</li>
                            </ul>
                        </div>
                    </div>
                </div>
				<div style="color:#fff">
					<?=$model->text?>
				</div>
                @if($model->tableproducts)
                    @foreach($model->tableproducts as $tableproducts)
                        <div class="card-table wow slideInUp inc0" data-wow-duration="1.5s">
							<?=$tableproducts->table?>
						</div>
                    @endforeach
                @endif

            </div>
        </div>
    </main>
    <!--END MAIN-->
    </div>

@endsection
