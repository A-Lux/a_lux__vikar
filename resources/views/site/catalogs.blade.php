@extends('layouts.main')

@section('content')

<main class="main">
	<div class="container">
		<div class="main-title catal-main-title">
			<h3>Любой <span>вид печати</span></h3>
		</div>

		<div class="main-content">
			<div class="products">
				@foreach($catalogs as $v)
				<div class="vivesky-item">
					<a href="/product/{{$url}}/{{$v->url}}" class="product-card wow fadeInUp" data-wow-duration="2s">
						<div class="product-title">
							@php
							$image = json_decode($v->img);
							@endphp
							<p>{{$v->name}}</p>
						</div>
						<div class="product-image">
							<img src="{{Voyager::image($image[0])}}" alt="">
							<img class="vikar" src="/images/vikar.png" alt="">
						</div>
						<div class="price-title">
							<p>{{$v->text_opisani}}</p>
							<div class="price">
								<p><span>{{$v->sum}}</span> Тг./кв.м.</p>
							</div>
						</div>
					</a>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</main>
<!--END MAIN-->

@endsection