<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="//yastatic.net/jquery/1.8.3/jquery.min.js"></script>
<div id="map" style="width: 80%; height: 600px; float:left;"></div>
<div id="viewContainer"></div>
<script>
    function init () {
        var myMap = new ymaps.Map('map', {
                center: [43.238286, 76.945456],
                zoom: 12,
                //controls: [routeTypeSelector]
            }, {
                buttonMaxWidth: 300
            });

        myMap.geoObjects.add(new ymaps.Placemark([43.238286, 76.945456], {
            balloonContent: ''
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }));

        myMap.events.add('click', function (e) {
            myMap.geoObjects.removeAll();
            var coords = e.get('coords');

            myMap.geoObjects.add(new ymaps.Placemark(coords, {
                balloonContent: ''
            }, {
                preset: 'islands#icon',
                iconColor: '#0095b6'
            }));

            // Создаем модель мультимаршрута.
            var multiRouteModel = new ymaps.multiRouter.MultiRouteModel([
                    [43.238286, 76.945456],
                    coords
                ], {
                    // Путевые точки можно перетаскивать.
                    // Маршрут при этом будет перестраиваться.
                    //wayPointDraggable: true,
                    boundsAutoApply: true
                });

            ymaps.modules.require([
                'MultiRouteCustomView'
            ], function (MultiRouteCustomView) {
                // Создаем экземпляр текстового отображения модели мультимаршрута.
                // см. файл custom_view.js
                new MultiRouteCustomView(multiRouteModel);
            });

            // Создаем карту с добавленной на нее кнопкой.


                // Создаем на основе существующей модели мультимаршрут.
                multiRoute = new ymaps.multiRouter.MultiRoute(multiRouteModel, {
                    // Путевые точки можно перетаскивать.
                    // Маршрут при этом будет перестраиваться.
                    //wayPointDraggable: true,
                    boundsAutoApply: true
                });
            // multiRoute.model.events.add("requestsuccess", function(){
            //     var a = multiRoute.model.getRoutes();
            //     console.log(a);
            // });
            // Добавляем мультимаршрут на карту.
            myMap.geoObjects.add(multiRoute);
            multiRouteModel.setParams({ routingMode: 'auto' }, true);
        });
    }

    ymaps.ready(init);

    ymaps.modules.define('MultiRouteCustomView', [
        'util.defineClass'
    ], function (provide, defineClass) {
        // Класс простого текстового отображения модели мультимаршрута.
        function CustomView (multiRouteModel) {
            this.multiRouteModel = multiRouteModel;
            // Объявляем начальное состояние.
            this.state = "init";
            this.stateChangeEvent = null;
            // Элемент, в который будет выводиться текст.
            $('#viewContainer').html('');
            this.outputElement = $('<div></div>').appendTo('#viewContainer');

            this.rebuildOutput();

            // Подписываемся на события модели, чтобы
            // обновлять текстовое описание мультимаршрута.
            multiRouteModel.events
                .add(["requestsuccess", "requestfail", "requestsend"], this.onModelStateChange, this);
        }

        // Таблица соответствия идентификатора состояния имени его обработчика.
        CustomView.stateProcessors = {
            init: "processInit",
            requestsend: "processRequestSend",
            requestsuccess: "processSuccessRequest",
            requestfail: "processFailRequest"
        };

        // Таблица соответствия типа маршрута имени его обработчика.
        CustomView.routeProcessors = {
            "driving": "processDrivingRoute",
            "masstransit": "processMasstransitRoute",
            "pedestrian": "processPedestrianRoute"
        };

        defineClass(CustomView, {
            // Обработчик событий модели.
            onModelStateChange: function (e) {
                // Запоминаем состояние модели и перестраиваем текстовое описание.
                this.state = e.get("type");
                this.stateChangeEvent = e;
                this.rebuildOutput();
            },

            rebuildOutput: function () {
                // Берем из таблицы обработчик для текущего состояния и исполняем его.
                var processorName = CustomView.stateProcessors[this.state];
                this.outputElement.html(
                    this[processorName](this.multiRouteModel, this.stateChangeEvent)
                );
            },

            processInit: function () {
                return "Инициализация ...";
            },

            processRequestSend: function () {
                return "Запрос данных ...";
            },

            processSuccessRequest: function (multiRouteModel, e) {
                var routes = multiRouteModel.getRoutes(),
                    // result = ["Данные успешно получены."];
                    result = [];
                if (routes.length) {
                    result.push("Всего маршрутов: " + routes.length + ".");
                    for (var i = 0, l = routes.length; i < l; i++) {
                        result.push(this.processRoute(i, routes[i]));
                    }
                } else {
                    result.push("Нет маршрутов.");
                }
                return result.join("<br/>");
            },

            processFailRequest: function (multiRouteModel, e) {
                return e.get("error").message;
            },

            processRoute: function (index, route) {
                // Берем из таблицы обработчик для данного типа маршрута и применяем его.
                var processorName = CustomView.routeProcessors[route.properties.get("type")];
                return (index + 1) + ". " + this[processorName](route);
            },

            processDrivingRoute: function (route) {
                // var result = ["Автомобильный маршрут."];
                var result = [];
                result.push(this.createCommonRouteOutput(route));
                return result.join("<br/>");
            },

            processMasstransitRoute: function (route) {
                var result = ["Маршрут на общественном транспорте."];
                result.push(this.createCommonRouteOutput(route));
                result.push("Описание маршута: <ul>" + this.createMasstransitRouteOutput(route) + "</ul>");
                return result.join("<br/>");
            },

            processPedestrianRoute: function (route) {
                var result = ["Пешеходный маршрут."];
                result.push(this.createCommonRouteOutput(route));
                return result.join("<br/>");
            },

            // Метод, формирующий общую часть описания для всех типов маршрутов.
            createCommonRouteOutput: function (route) {
                // return "Протяженность маршрута: " + route.properties.get("distance").text + "<br/>" +
                //     "Время в пути: " + route.properties.get("duration").text;
                return "Протяженность маршрута: " + route.properties.get("distance").text;
            },

            // Метод, строящий список текстовых описаний для
            // всех сегментов маршрута на общественном транспорте.
            createMasstransitRouteOutput: function (route) {
                var result = [];
                for (var i = 0, l = route.getPaths().length; i < l; i++) {
                    var path = route.getPaths()[i];
                    for (var j = 0, k = path.getSegments().length; j < k; j++) {
                        result.push("<li>" + path.getSegments()[j].properties.get("text") + "</li>");
                    }
                }
                return result.join("");
            },

            destroy: function () {
                this.outputElement.remove();
                this.multiRouteModel.events
                    .remove(["requestsuccess", "requestfail", "requestsend"], this.onModelStateChange, this);
            }
        });

        provide(CustomView);
    });


</script>
