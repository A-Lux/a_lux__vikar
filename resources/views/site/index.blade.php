@extends('layouts.main_index')

@section('content')

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>VIKAR</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" href="css/glide.core.min.css">
  <link rel="stylesheet" href="css/glide.theme.min.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/style.css">

</head>

<!--MAIN-->
<main class="main">

  <div class="main-callback-message-btn">
    <a href="#">
      <img src="images/callback-mesage.png" alt="Обратная связь">
    </a>
  </div>

  <div class="main-callback-block">
    <div class="main-callback-top">
      <div class="main-callback-top-img">
        <img src="images/callback-mesage-black.png" alt="Обратная связь">
      </div>

      <div class="main-callback-top-text">
        <h5>Ирина</h5>
        <span>Консультант</span>
      </div>

      <div class="main-callback-close-wrap">
        <span class="main-callback-close">
          <img src="images/main-close.png" alt="Закрыть">
        </span>
      </div>

    </div>

    <div class="main-callback-content">
      <div class="main-callback-content-text">
        <p>Мы рады приветствовать Вас на нашем сайте!</p>
        <p>Если Вас что-то заинтересовало - Вы можете написать нам.</p>
      </div>
      <span class="main-callback-content-date">15:35</span>
      <textarea name="" id="" class="main-callback-textarea" placeholder="Введите сообщение и нажмите кнопку отправить"
        cols="34" rows="2"></textarea>
    </div>

    <div class="main-callback-send-btn">
      <a href="#">Отправить сообщение</a>
    </div>
  </div>

  <div class="main-first-section">

    <div class="container">
      <div class="main-cap">
        <div class="main-cap-logo">
          <img src="images/logo-cap.png" alt="Логотип">
        </div>

        <div class="main-cap-right">
          <div class="main-cap-phone">
            <img src="images/main-cap-phone.png" alt="Телефон">
            <h3>+7 (727) 739-335-79</h3>
          </div>

          <div class="main-cap-message">
            <img src="images/main-cap-message.png" alt="Email">
            <h5>Email:</h5>
            <a href="#">vikarsupport@mail.ru</a>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid gallery-desktop px-5">
      <div class="container px-5">
        <div class="row px-5">
          <div class="col-sm-3  p-2 ">
            <div class="gallery-1 photo1" id="gallery1-1">

            </div>
          </div>
          <div class="col-sm-3  p-2">
            <div class="gallery-1 photo2" id="gallery1-2">

            </div>
          </div>
          <div class="col-sm-3  p-2">
            <div class="gallery-1 photo3" id="gallery1-3">

            </div>
          </div>
          <div class="col-sm-3  p-2">
            <div class="gallery-1 photo4" id="gallery1-4">

            </div>
          </div>
        </div>
        <div class="row px-5">
          <div class="col-sm-3 p-2">
            <div class="gallery-1 photo5" id="gallery1-5">

            </div>
          </div>
          <div class="col-sm-3 p-2">
            <div class="gallery-1 photo6" id="gallery1-6">

            </div>
          </div>
          <div class="col-sm-3 p-2">
            <div class="gallery-1 photo7" id="gallery1-7">

            </div>
          </div>
          <div class="col-sm-3 p-2">
            <div class="gallery-1 photo8" id="gallery1-8">

            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid  mobile-slider">
      <div class="glide">
        <div class="glide__track" data-glide-el="track">
          <ul class="glide__slides">
            <li class="glide__slide">
                <div class="col-sm-3  p-2">
                    <div class="gallery-1 photo1" id="gallery1-1-mob">
        
                    </div>
                  </div>
            </li>
            <li class="glide__slide">
                <div class="col-sm-3  p-2">
                    <div class="gallery-1 photo2" id="gallery1-2-mob">
        
                    </div>
                  </div>
            </li>
            <li class="glide__slide">
                <div class="col-sm-3  p-2">
                    <div class="gallery-1 photo3" id="gallery1-3-mob">
        
                    </div>
                  </div>
            </li>
            <li>
                <div class="col-sm-3  p-2">
                    <div class="gallery-1 photo4" id="gallery1-4-mob">
        
                    </div>
                  </div>
            </li>
            <li>
                <div class="col-sm-3  p-2">
                    <div class="gallery-1 photo5" id="gallery1-5-mob">
        
                    </div>
                  </div>
            </li>
            <li>
                <div class="col-sm-3  p-2">
                    <div class="gallery-1 photo6" id="gallery1-6-mob">
        
                    </div>
                  </div>
            </li>
            <li>
                <div class="col-sm-3  p-2">
                    <div class="gallery-1 photo7" id="gallery1-7-mob">
        
                    </div>
                  </div>
            </li>
            <li>
                <div class="col-sm-3  p-2">
                    <div class="gallery-1 photo8" id="gallery1-8-mob">
        
                    </div>
                  </div>
            </li>
          </ul>
        </div>
        <div class="glide__arrows" data-glide-el="controls">
            <button class="glide__arrow glide__arrow--left" data-glide-dir="<"></button>
            <button class="glide__arrow glide__arrow--right" data-glide-dir=">"></button>
          </div>
      </div>
    </div>
    <!-- <div class="container-fluid">
        <div class="main-images wow zoomIn">
          <img src="images/main-layer-1.jpg" alt="main-layer-1">
          <img src="images/main-layer-2.jpg" alt="main-layer-2">
          <img src="images/main-layer-3.jpg" alt="main-layer-3">
          <img src="images/main-layer-4.jpg" alt="main-layer-4">
          <img src="images/main-layer-5.jpg" alt="main-layer-5">
          <img src="images/main-layer-6.jpg" alt="main-layer-6">
          <img src="images/main-layer-7.jpg" alt="main-layer-7">
          <img src="images/main-layer-8.jpg" alt="main-layer-8">
          <img src="images/main-layer-9.jpg" alt="main-layer-9">
          <img src="images/main-layer-10.jpg" alt="main-layer-10">
          <img src="images/main-layer-11.jpg" alt="main-layer-11">
          <img src="images/main-layer-12.jpg" alt="main-layer-12">
          <img src="images/main-layer-13.jpg" alt="main-layer-13">
        </div>
      </div>
    </div>-->

    <div class="main-greeting">
      <div class="container">
        <div class="row">
          <div class="col main-greeting-title wow slideInLeft">
            <h1>Привет <span class="red-text">коллега!</span></h1>
            <h3>Рады, что Вы к нам заглянули.</h3>
          </div>
        </div>

        <div class="row">
          <div class="col main-greeting-mini-title wow slideInRight">
            <h4>Ищете рекламное агентство?</h4>
            <p><span class="bold-26">Старые</span> подрядчики <span class="bold-24">ломят</span> цены? Не выдерживают
              необходимые <span class="bold-26">сроки ?</span> <br>
              Ваше бывшее рекламное агентство <span class="bold-26">не отстреливает</span> тренды? <br><span
                class="bold-26">Времени</span>
              на поиски подрядчиков просто <span class="bold-26">нет?</span></p>
          </div>
        </div>

        <div class="row">
          <div class="col main-greeting-btn wow slideInLeft">
            <a href="#">Скачать презентацию</a>
            <a href="#">Скачать прайс-лист</a>
          </div>
        </div>

        <div class="row">
          <div class="col main-greeting-mini-title wow slideInRight">
            <h4>Тогда вы правильно сделали, что заглянули именно к нам!</h4>
            <p>Мы <span class="bold-26">готовы</span> решать поставленные Вами в задачи в те <span
                class="bold-26">СРОКИ</span>
              <br>
              в которые <span class="bold-26">ВАМ</span> они необходимы! <br>
              Предложить ту стоимость, чтобы она <span class="bold-26">Вас радовала!</span></p>
          </div>
        </div>
      </div>
    </div>

    <div class="main-last-section">
      <div class="container-fluid">
        <div class="main-last-wrap">
          <div class="main-links-wrap">
            <!--
            <div class="main-link-price_list">
              <img src="images/main-price_list.jpg" alt="Прайс лист">
              <a href="/price-list">Прайс лист</a>
            </div>

            <div class="main-link-products_services">
              <img src="images/main-calc_tea.jpg" alt="Товары и услуги">
              <a href="/goods-and-services">Товары и услуги <br> с онлайн- <br> калькулятором</a>
            </div>

            <div class="main-link-delivery">
              <img src="images/main-delivery.jpg" alt="Доставка">
              <a href="/shipping-and-payment">Доставка</a>
            </div>

            <div class="main-link-photo_report">
              <img src="images/main-photocamera.jpg" alt="Фото-Видео отчет">
              <a href="/photo-video-report">Фото <br> видео <br> отчет</a>
            </div>

            <div class="main-link-contacts">
              <img src="images/main-phone.jpg" alt="Контакты">
              <a href="/contact-details">Контакты</a>
            </div>
            -->
          </div>

          <div class="container-fluid">
            <div class="container">
              <div class="row">
                <div class="col-sm-3  p-2">
                  <div class="gallery-1 photo2-1" style="background: url(/public/images/layer-1.jpg);" id="gallery2-1">

                  </div>
                </div>
                <div class="col-sm-3  p-2">
                  
                  <div class="gallery-1 photo2-2" style=" background: url(/public/images/main-price_list.jpg);"
                    id="gallery2-2"><a href="/goods-and-services" style="position:absolute; width:100%; height: 100%;"></a>
                    
                    <a href="/goods-and-services">Товары и услуги <br> с онлайн- <br> калькулятором</a>
                  </div>
                
                </div>
                <div class="col-sm-3  p-2">
                  <div class="gallery-1 photo2-3" style="background: url(/public/images/layer-3.jpg);" id="gallery2-3">

                  </div>
                </div>
                <div class="col-sm-3  p-2">
                  <div class="gallery-1 photo2-4" style=" background: url(/public/images/main-calc_tea.jpg);"
                    id="gallery2-4"><a href="/photo-video-report" style="position:absolute; width:100%; height: 100%;"></a>
                    <a href="/photo-video-report">Фото <br> видео <br> отчет</a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-3 p-2">
                  <div class="gallery-1 photo2-5" style="background: url(/public/images/main-delivery.jpg);"
                    id="gallery2-5"><a href="/price-list" style="position:absolute; width:100%; height: 100%;"></a>
                    <a href="/price-list">Прайс лист</a>
                  </div>
                </div>
                <div class="col-sm-3 p-2">
                  <div class="gallery-1 photo2-6" style="background: url(/public/images/layer-6.jpg);" id="gallery2-6">

                  </div>
                </div>
                <div class="col-sm-3 p-2">
                  <div class="gallery-1 photo2-7" style="background: url(/public/images/main-photocamera.jpg);"
                    id="gallery2-7"><a href="/shipping-and-payment" style="position:absolute; width:100%; height: 100%;"></a>
                    <a href="/shipping-and-payment">Доставка</a>
                  </div>
                </div>
                <div class="col-sm-3 p-2">
                  <div class="gallery-1 photo2-8" style="background: url(/public/images/layer-7.jpg);" id="gallery2-8">

                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- <div class="main-last-images wow fadeInUp">
            <img src="images/layer-1.jpg" alt="layer-1">
            <img src="images/layer-2.jpg" alt="layer-2">
            <img src="images/layer-3.jpg" alt="layer-3">
            <img src="images/layer-4.jpg" alt="layer-4">
            <img src="images/layer-5.jpg" alt="layer-5">
            <img src="images/layer-6.jpg" alt="layer-6">
            <img src="images/layer-7.jpg" alt="layer-7">
            <img src="images/layer-8.jpg" alt="layer-8">
            <img src="images/layer-9.jpg" alt="layer-9">
          </div>
        </div>-->

        </div>
      </div>
    </div>
</main>
<!--END MAIN-->

@endsection