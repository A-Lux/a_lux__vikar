@extends('layouts.main')

@section('content')
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <!--MAIN-->
    <main class="main">
        <div class="container">
            <div class="main-left-title">
                <h3>{{$model->name}}</h3>
            </div>

            <div class="main-content">
                <div class="row">
                    <div class="col-lg-6">

                        <div id="calc-block" class="calc-img fotorama" data-nav="thumbs" data-arrows="true">
                            @php
                                $images = json_decode($model->img);
                            @endphp
                            @if(count($images))
                                @foreach($images as $v)
                                    <a href="/storage/{{$v}}"><img src="/storage/{{$v}}" class="img-fluid" alt="img"></a>
                                    <img src="" class="img-fluid" alt="">sss
                                @endforeach
                            @endif
                        </div>

                        {!! $model->text !!}
                    </div>

                    <div class="col-lg-6 wow fadeInRightBig">
                        <form action="/oformit_zacaz2">
                            <div class="calc-bg">

                                <div class="calc-dark-text">
                                    Световой короб "Прямой"
                                </div>
                                <div class="number-input operations-block">
                                    <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation" >-</span>
                                    <input class="quantity text-center form-control input-text quantity_sm" min="0" name="quantity_sm" value="50" type="number">
                                    <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus btn-operation">+</span>
                                </div>
                                <div class="scale-text">
                                    Высота, см
                                </div>
                                <div class="number-input operations-block">
                                    <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation" >-</span>
                                    <input class="quantity text-center form-control input-text quantity_col" min="0" name="quantity_col" value="50" type="number">
                                    <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus btn-operation">+</span>
                                </div>
                                <div class="scale-text sm">
                                    Количество
                                </div>
                                <input type="hidden" value="{{$model->id}}" name="id">
                                <input type="hidden" value="{{$model->calcs[0]->price}}" name="price_sm">
                                <input type="hidden" value="{{$model->calcs[1]->price}}" name="price_col">
                                <div class="checkbox-block checkbox-block1">
                                    <div class="label-text">
                                        Выберите вариант подсветки
                                    </div>
                                    <input checked name="type" value="{{$model->calcs[2]->calc_id}}" type="radio" data-val="{{$model->calcs[2]->price}}">Лицевая подсветка
									<a href="/storage/productpriceimg/{{$model->id}}/{{$model->calcs[2]->img}}"><img src="/images/info.png" alt="info"></a>
                                    <br>
                                    <input name="type" value="{{$model->calcs[3]->calc_id}}" type="radio" data-val="{{$model->calcs[3]->price}}">Контражурная подсветка
									<a href="/storage/productpriceimg/{{$model->id}}/{{$model->calcs[3]->img}}"><img src="/images/info.png" alt="info"></a>
                                    <br>
                                    <input name="type" value="{{$model->calcs[4]->calc_id}}" type="radio" data-val="{{$model->calcs[4]->price}}">Лицевая + контражурная подсветка
									<a href="/storage/productpriceimg/{{$model->id}}/{{$model->calcs[4]->img}}"><img src="/images/info.png" alt="info"></a>
                                    <br>
                                    <input name="type" value="{{$model->calcs[5]->calc_id}}" type="radio" data-val="{{$model->calcs[5]->price}}">Лицевая + боковая подсветка
									<a href="/storage/productpriceimg/{{$model->id}}/{{$model->calcs[5]->img}}"><img src="/images/info.png" alt="info"></a>
                                    <br>
                                    <input name="type" value="{{$model->calcs[6]->calc_id}}" type="radio" data-val="{{$model->calcs[6]->price}}">Без подсветки
									<a href="/storage/productpriceimg/{{$model->id}}/{{$model->calcs[6]->img}}"><img src="/images/info.png" alt="info"></a>
                                </div>

                                <div class="checkbox-block checkbox-block2">
                                    <div class="label-text">
                                        Яркость подсветки
                                    </div>
                                    <input checked name="yarkost" value="{{$model->calcs[7]->calc_id}}" type="radio" data-val="{{$model->calcs[7]->price}}">Повышенная
									<a href="/storage/productpriceimg/{{$model->id}}/{{$model->calcs[7]->img}}"><img src="/images/info.png" alt="info"></a>
                                    <input name="yarkost" value="{{$model->calcs[8]->calc_id}}" type="radio" data-val="{{$model->calcs[8]->price}}">Стандартная
									<a href="/storage/productpriceimg/{{$model->id}}/{{$model->calcs[8]->img}}"><img src="/images/info.png" alt="info"></a>
                                </div>
                                <div class="checkbox-block checkbox-block2">
                                    <div class="label-text">
                                        Дополнительные настройки
                                    </div>
                                    <input checked name="dop" value="{{$model->calcs[9]->calc_id}}" type="radio" data-val="{{$model->calcs[9]->price}}">Сложный шрифт (рукописный, засечки)
									<a href="/storage/productpriceimg/{{$model->id}}/{{$model->calcs[9]->img}}"><img src="/images/info.png" alt="info"></a>
                                    <br>
                                    <input name="dop" value="{{$model->calcs[10]->calc_id}}" type="radio" data-val="{{$model->calcs[10]->price}}">Легкая металлическая рама
									<a href="/storage/productpriceimg/{{$model->id}}/{{$model->calcs[10]->img}}"><img src="/images/info.png" alt="info"></a>
                                </div>

                                <div class="label-text">
                                    Добавить комментарий
                                </div>
                                <textarea name="" id="" rows="3" class="form-control"></textarea>
                            </div>
                            <div class="button-block">
                                <a href="" class="btn price-button">Ваша цена: <span>
                                        {{$model->calcs[0]->price * 50 + $model->calcs[1]->price * 50 + $model->calcs[2]->price + $model->calcs[7]->price
                                        + $model->calcs[9]->price}} тг.
                                    </span></a>
                                <a href="" class="btn send-button oformit_zacaz2">Оформить заказ</a>
                            </div>

                        </form>
                    </div>

                </div>

                @if($model->tableproducts)
                    @foreach($model->tableproducts as $tableproducts)
                        <div class="card-table wow slideInUp inc0" data-wow-duration="1.5s">
                            {!! $tableproducts->table !!}
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </main>
    <!--END MAIN-->
    </div>

@endsection
