@extends('layouts.main')

@section('content')

    <div class="content">
        <!--MAIN-->
        <main class="main">
            <div class="container">
                <div class="main-left-title">
                    <h3 class="vakansii">Вакансии</h3>
                </div>

                <div class="main-content">
                    @foreach($jobs as $k => $v)
                        <div class="block-vakansiya wow @if($k == 0) slideInLeft @elseif($k == 1) slideInRight @else slideInUp @endif" data-wow-duration="1.5s">
                            <div class="container-main-vakansiya">
                                <h4>{!! $v->title !!}</h4>
                            </div>
                            <div class="container-price">
                                <p class="price">{!! $v->price !!}</p>
                            </div>
                            <div class="vakansii-content">
                                {!! $v->content !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </main>
        <!--END MAIN-->
    </div>

@endsection
