@extends('layouts.main')

@section('content')

    <div class="content">

        <!--MAIN-->
        <main class="main">
            <div class="container">
                <div class="main-left-title">
                    <h3 class="text-center">доставка</h3>
                </div>
                <div class="main-content">
                    <div class="col-sm-12">
                        <div class="delivery-text">
                            <p>Способы доставки:</p>
                            <ul>
                                <li>Самовывоз: В рабочие дни с 9 до 18.00</li>
                                <li>Транспортная компания	</li>
                                <li>Доставка нашим транспортом:</li>
                            </ul>
                        </div>
                        <div class="frame">
                            <div class="first-head">
                                <div class="cost-delivery">
                                    <p>Стоимость доставки:</p>
                                </div>
                                <div class="select" style="margin-top: 13px;">
                                    <input type="text" class="address" style="float: left;">
                                    <button class="address_search" style="cursor: pointer;float: left;">Искать</button>
                                    <div class="itog_address" style="float: left;margin:3px 0 0 10px;"></div>
                                    {{--<select>--}}
                                        {{--<option>Выберите район доставки</option>--}}
                                        {{--<option>Бостандыкский</option>--}}
                                        {{--<option>Ауезовский</option>--}}
                                        {{--<option>Турксибский</option>--}}
                                        {{--<option>Медеуский</option>--}}
                                        {{--<option>Алмалинский</option>--}}
                                        {{--<option>Алатауский</option>--}}
                                    {{--</select>--}}
                                    <div class="select_arrow">
                                        <img src="images/cost-arrow.png" alt="">
                                    </div>
                                </div>
                                <div class="del-text-block">
                                    <div class="cost-text">
                                        Цена доставки: <span>Бесплатно</span>
                                    </div>
                                    <div class="free-del-text">
                                        *Бесплатная доставка свыше <span>200 00 руб.</span>
                                    </div>
                                </div>
                            </div>
                            <script>
                                var deliveries = [];
                            </script>
                            <div class="second-head">
                                <ul>
                                    @foreach($deliveries as $v)
                                        <script>
                                            deliveries[{{$v->radius}}] = {{$v->price}};
                                        </script>
                                        <li><div style="margin-right: 7px;margin-top:5px; border-radius: 10px;float: left; width: 15px; height: 15px; background-color: #{{$v->color}}"></div>Доставка {{$v->price}} рублей</li>
                                    @endforeach
                                    {{--<li><img src="images/li2.png" alt="">Доставка 500 рублей</li>--}}
                                    {{--<li><img src="images/li3.png" alt="">Доставка 800 рублей</li>--}}
                                    {{--<li><img src="images/li4.png" alt="">Доставка 1000 рублей</li>--}}
                                    {{--<li><img src="images/li5.png" alt="">Бесплатная доставка</li>--}}
                                </ul>
                            </div>
                            <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                            <div id="map" style="width: 100%; height: 600px"></div>
                        </div>
                        <div class="main-left-title space">
                            <h3 class="text-center">оплата</h3>
                        </div>
                        <div class="payment-text">
                            <ul>
                                <li class="left"><div class="head">
                                        Оплата через платежную систему QIWI
                                    </div>
                                    <p>Оплата наличными через терминалы и кошелек QIWI – без
                                        комиссий. Моментальное поступление денежных средств. Ссылка
                                        на QIWI кошелек</p>
                                </li>
                                <li class="left"><div class="head">
                                        Наличными Курьеру
                                    </div>
                                    <p>Данный способ оплаты возможен только для жителей города
                                        Алматы, воспользовавшихся услугой Бесплатной доставки.</p>
                                </li>
                                <li class="right"><div class="head">
                                        Банковский перевод на расчетный счет
                                    </div>
                                    <p>Оплата производится через отделение банков по
                                        выставленному счету (поступление суммы в течение одного
                                        рабочего дня).</p>
                                </li>
                                <li class="right"><div class="head">
                                        Онлайн-оплата
                                    </div>
                                    <p>Впишите свой адрес в поле выше, и узнайте стоимость доставки.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!--END MAIN-->
    </div>

    <script>
        ymaps.ready(init);
        var myPlacemark = '';

        function init() {
            // Создаем карту.
            var myMap = new ymaps.Map("map", {
                center: [{{$map->x}}, {{$map->y}}],
                zoom: 12
            }, {
                searchControlProvider: 'yandex#search'
            });
            // Создаем круг.
            @foreach($deliveries as $v)
                var myCircle = new ymaps.Circle([
                    // Координаты центра круга.
                    [{{$map->x}}, {{$map->y}}],
                    // Радиус круга в метрах.
                    {{$v->radius}}
                ], {
                    // Описываем свойства круга.
                    // Содержимое балуна.
                    //balloonContent: "Радиус круга - 10 км",
                    // Содержимое хинта.
                    //hintContent: "Подвинь меня"
                }, {
                    // Задаем опции круга.
                    // Включаем возможность перетаскивания круга.
                    // Цвет заливки.
                    // Последний байт (77) определяет прозрачность.
                    // Прозрачность заливки также можно задать используя опцию "fillOpacity".
                    fillColor: "#{{$v->color}}",
                    // Цвет обводки.
                    strokeColor: "#{{$v->strokeColor}}",
                    // Прозрачность обводки.
                    strokeOpacity: {{$v->strokeOpacity}},
                    // Ширина обводки в пикселях.
                    strokeWidth: {{$v->strokeWidth}}
                });

                // Добавляем круг на карту.
                myMap.geoObjects.add(myCircle);
            @endforeach

            myMap.geoObjects.add(new ymaps.Placemark([{{$map->x}}, {{$map->y}}], {
                    balloonContent: ''
                }, {
                    preset: 'islands#icon',
                    iconColor: '#0095b6'
                }));

            {{--myMap.events.add('click', function (e) {--}}
                {{--var coords = e.get('coords');--}}

                {{--var p2 = myMap.geoObjects.add(new ymaps.Placemark(coords, {--}}
                    {{--balloonContent: ''--}}
                {{--}, {--}}
                    {{--preset: 'islands#icon',--}}
                    {{--iconColor: '#0095b6'--}}
                {{--}));--}}

                {{--alert(ymaps.coordSystem.geo.distance([{{$map->x}}, {{$map->y}}],coords));--}}
            {{--});--}}
            $('.address_search').on('click', function () {
                var searchQuery = $('.address').val()+ ' Алматы';
                ymaps.geocode(searchQuery, {results: 1}, {results: 100}).then(function (res) {
                    var geoObject = res.geoObjects.get(0);
                    if (!geoObject) {
                        alert('Not found');
                        return;
                    }else{
                        var cords = geoObject.geometry.getCoordinates();

                        var distance = '';
                        for(key in deliveries){
                            if(parseInt(ymaps.coordSystem.geo.distance([{{$map->x}}, {{$map->y}}],[cords[0], cords[1]])) <= parseInt(key)) {
                                distance = deliveries[key];
                                break;
                            }
                        }

                        if(distance)
                            $('.itog_address').html(distance+' тг.');
                        else
                            $('.itog_address').html('');

                        myMap.geoObjects.add(new ymaps.Placemark([{{$map->x}}, {{$map->y}}], {
                            balloonContent: ''
                        }, {
                            preset: 'islands#icon',
                            iconColor: '#0095b6'
                        }));

                        myMap.geoObjects.remove(myPlacemark);

                        myPlacemark = new ymaps.Placemark([cords[0], cords[1]], {
                            balloonContent: ''
                        }, {
                            preset: 'islands#icon',
                            iconColor: '#0095b6'
                        });

                        myMap.geoObjects.add(myPlacemark);
                    }
                });
            });

            {{--alert(ymaps.coordSystem.geo.distance([{{$map->x}}, {{$map->y}}],coords));--}}
        }
    </script>

 @endsection
