@extends('layouts.main')

@section('content')
    @php
        $deliveries = \App\Delivery::orderBy('radius', 'ASC')->get();
        $map = \App\Map::first();
    @endphp
    <div class="overlay" style="display: none;">
        <div class="feedback" >
			<div class="close-modal">
                 &#10005
            </div>
            <div class="col-sm-12">
                <div class="">
                    <div class="first-head">
                        <div class="cost-delivery">
                            <p>Стоимость доставки:</p>
                        </div>
                        <div class="select" style="margin-top: 13px;">
                            <input type="text" class="address" style="float: left;">
                            <button class="address_search" style="cursor: pointer;float: left;">Искать</button>
                            <div class="itog_address" style="float: left;margin:3px 0 0 10px;"></div>
                            <div class="select_arrow">
                                <img src="images/cost-arrow.png" alt="">
                            </div>
                        </div>
                        <div class="del-text-block">
                            <div class="cost-text">
                                Цена доставки: <span>Бесплатно</span>
                            </div>
                            <div class="free-del-text">
                                *Бесплатная доставка свыше <span>200 00 руб.</span>
                            </div>
                        </div>
                    </div>
                    <script>
                        var deliveries = [];
                    </script>
                    <div class="second-head">
                        <ul>
                            @foreach($deliveries as $v)
                                <script>
                                    deliveries[{{$v->radius}}] = {{$v->price}};
                                </script>
                                <li><div style="margin-right: 7px;margin-top:5px; border-radius: 10px;float: left; width: 15px; height: 15px; background-color: #{{$v->color}}"></div>Доставка {{$v->price}} рублей</li>
                            @endforeach
                        </ul>
                    </div>
                    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                    <div id="map" class="delivery_map"></div>
                </div>
            </div>
            <script>
                ymaps.ready(init);
                var myPlacemark = '';

                function init() {
                    // Создаем карту.
                    var myMap = new ymaps.Map("map", {
                        center: [{{$map->x}}, {{$map->y}}],
                        zoom: 12
                    }, {
                        searchControlProvider: 'yandex#search'
                    });
                    // Создаем круг.
                            @foreach($deliveries as $v)
                    var myCircle = new ymaps.Circle([
                            // Координаты центра круга.
                            [{{$map->x}}, {{$map->y}}],
                            // Радиус круга в метрах.
                                {{$v->radius}}
                        ], {
                        }, {
                            fillColor: "#{{$v->color}}",
                            // Цвет обводки.
                            strokeColor: "#{{$v->strokeColor}}",
                            // Прозрачность обводки.
                            strokeOpacity: {{$v->strokeOpacity}},
                            // Ширина обводки в пикселях.
                            strokeWidth: {{$v->strokeWidth}}
                        });

                    // Добавляем круг на карту.
                    myMap.geoObjects.add(myCircle);
                    @endforeach

                    myMap.geoObjects.add(new ymaps.Placemark([{{$map->x}}, {{$map->y}}], {
                        balloonContent: ''
                    }, {
                        preset: 'islands#icon',
                        iconColor: '#0095b6'
                    }));

                    $('.address_search').on('click', function () {
                        var searchQuery = $('.address').val()+ ' Алматы';
                        ymaps.geocode(searchQuery, {results: 1}, {results: 100}).then(function (res) {
                            var geoObject = res.geoObjects.get(0);
                            if (!geoObject) {
                                alert('Not found');
                                return;
                            }else{
                                var cords = geoObject.geometry.getCoordinates();

                                var distance = '';
                                for(key in deliveries){
                                    if(parseInt(ymaps.coordSystem.geo.distance([{{$map->x}}, {{$map->y}}],[cords[0], cords[1]])) <= parseInt(key)) {
                                        distance = deliveries[key];
                                        break;
                                    }
                                }

                                if(distance) {
                                    $('.itog_address').html(distance + ' тг.');
                                    itog_address = distance;
                                }
                                else
                                    $('.itog_address').html('');

                                myMap.geoObjects.add(new ymaps.Placemark([{{$map->x}}, {{$map->y}}], {
                                    balloonContent: ''
                                }, {
                                    preset: 'islands#icon',
                                    iconColor: '#0095b6'
                                }));

                                myMap.geoObjects.remove(myPlacemark);

                                myPlacemark = new ymaps.Placemark([cords[0], cords[1]], {
                                    balloonContent: ''
                                }, {
                                    preset: 'islands#icon',
                                    iconColor: '#0095b6'
                                });

                                myMap.geoObjects.add(myPlacemark);
                            }
                        });
                    });
                }
            </script>
            <button class="end_map">Готово</button>
        </div>
    </div>
    {{--<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>--}}
    <!--MAIN-->
    <main class="main">
        <div class="container">
            <div class="main-left-title">
                <h3>Купить {{$model->name}}</h3>
            </div>

            <div class="main-content">
                @if($model->tableproducts)
                    @foreach($model->tableproducts as $k => $tableproducts)
                        <div class="card-table wow slideInUp inc{{$k}}" data-wow-duration="1.5s" style="cursor: pointer;">
                            {!! $tableproducts->table !!}
                        </div>
                    @endforeach
                @endif
                    <input type="hidden" value="{{$model->id}}" class="model_id">
                    <div class="card-table wow slideInUp pay_order" data-wow-duration="1.5s" style="display: none;">
                        <table style="height: 164px; width: 982px;">
                            <tbody>
                            <tr style="height: 20px;">
                                <td style="width: 131px; height: 20px;">Название</td>
                                <td>Количество</td>
                                <td style="width: 134px; height: 20px;">Цена, за ед.</td>
                                <td style="width: 133px; height: 20px;">Цена</td>
                                <td></td>
                            </tr>
                            <tr style="height: 19px;">
                                <td style="width: 131px; height: 19px;">Печать на банере</td>
                                <td><input type="text" value="1" class="col_order" style="width: 15%;"></td>
                                <td class="cena_ed"></td>
                                <td class="cena_ob"></td>
                                <td><input type="checkbox" checked class="p_banner"></td>
                            </tr>
                            <tr style="height: 21px;">
                                <td style="width: 131px; height: 19px;">Услуги дизайнера</td>
                                <td style="width: 131px; height: 21px;">1</td>
                                <td style="width: 134px; height: 21px;" class="c_dizainera_ed">3000</td>
                                <td style="width: 133px; height: 21px;" class="c_dizainera">3000</td>
                                <td><input type="checkbox" checked class="u_dizainera"></td>
                            </tr>
                            <tr style="height: 21px;">
                                <td style="width: 134px; height: 19px;">Доставка до адреса</td>
                                <td style="width: 131px; height: 21px;"></td>
                                <td style="width: 134px; height: 21px;"></td>
                                <td style="width: 133px; height: 21px;" class="dostavka"></td>
                                <td><input type="checkbox" class="order-btn mt-2" style="margin:auto;"></td>
                            </tr>
                            <tr style="height: 21px;">
                                <td style="width: 134px; height: 19px;"></td>
                                <td style="width: 131px; height: 21px;"></td>
                                <td style="width: 134px; height: 21px;">ИТОГО</td>
                                <td style="width: 133px; height: 21px;" class="itog"></td>
                                <td><input type="button" class="order " value="Оформить заказ" onclick="window.location.href = '/tableproducts';" ></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </main>
    <!--END MAIN-->
    </div>

@endsection
