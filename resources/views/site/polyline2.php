<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<div id="map" style="width: 100%; height: 600px"></div>
<script>
    ymaps.ready(init);

    function init() {
        // Создаем карту.
        var myMap = new ymaps.Map("map", {
            center: [43.238286, 76.945456],
            zoom: 12
        }, {
            searchControlProvider: 'yandex#search'
        });

        // var coords = [];
        // myMap.events.add('click', function (e) {
        //     //map.geoObjects.removeAll();
        //     coords.push(e.get('coords'));
        //
        //     var myPolyline = new ymaps.Polyline(
        //         // Указываем координаты вершин ломаной.
        //         coords
        //     , {
        //         // Описываем свойства геообъекта.
        //         // Содержимое балуна.
        //         balloonContent: "Ломаная линия"
        //     }, {
        //         // Задаем опции геообъекта.
        //         // Отключаем кнопку закрытия балуна.
        //         balloonCloseButton: false,
        //         // Цвет линии.
        //         strokeColor: "#000000",
        //         // Ширина линии.
        //         strokeWidth: 4,
        //         // Коэффициент прозрачности.
        //         strokeOpacity: 0.5
        //     });
        //
        //     // Добавляем линии на карту.
        //     myMap.geoObjects
        //         .add(myPolyline);
        //
        // });

        // Создаем круг.
        var myCircle = new ymaps.Circle([
            // Координаты центра круга.
            [43.238286, 76.945456],
            // Радиус круга в метрах.
            500
        ], {
            // Описываем свойства круга.
            // Содержимое балуна.
            balloonContent: "Радиус круга - 10 км",
            // Содержимое хинта.
            hintContent: "Подвинь меня"
        }, {
            // Задаем опции круга.
            // Включаем возможность перетаскивания круга.
            draggable: true,
            // Цвет заливки.
            // Последний байт (77) определяет прозрачность.
            // Прозрачность заливки также можно задать используя опцию "fillOpacity".
            fillColor: "#DB709377",
            // Цвет обводки.
            strokeColor: "#990066",
            // Прозрачность обводки.
            strokeOpacity: 0.8,
            // Ширина обводки в пикселях.
            strokeWidth: 5
        });

        // Добавляем круг на карту.
        myMap.geoObjects.add(myCircle);

        var myCircle = new ymaps.Circle([
            // Координаты центра круга.
            [43.238286, 76.945456],
            // Радиус круга в метрах.
            3000
        ], {
            // Описываем свойства круга.
            // Содержимое балуна.
            balloonContent: "Радиус круга - 3 км",
            // Содержимое хинта.
            //hintContent: "Подвинь меня"
        }, {
            // Задаем опции круга.
            // Включаем возможность перетаскивания круга.
            draggable: false,
            // Цвет заливки.
            // Последний байт (77) определяет прозрачность.
            // Прозрачность заливки также можно задать используя опцию "fillOpacity".
            fillColor: "#DB709570",
            // Цвет обводки.
            strokeColor: "#000",
            // Прозрачность обводки.
            strokeOpacity: 0.8,
            // Ширина обводки в пикселях.
            strokeWidth: 5
        });

        // Добавляем круг на карту.
        myMap.geoObjects.add(myCircle);

        myMap.events.add('click', function (e) {
            //myMap.geoObjects.removeAll();
            // var coords = e.get('coords');

            // myMap.geoObjects.add(new ymaps.Placemark(coords, {
            //     balloonContent: ''
            // }, {
            //     preset: 'islands#icon',
            //     iconColor: '#0095b6'
            // }));
            // myMap.setCenter(coords);

            // Создаем круг.
            var myCircle = new ymaps.Circle([
                // Координаты центра круга.
                [43.238286, 76.945456],
                // Радиус круга в метрах.
                500
            ], {
                // Описываем свойства круга.
                // Содержимое балуна.
                balloonContent: "Радиус круга - 500 м",
                // Содержимое хинта.
                //hintContent: "Подвинь меня"
            }, {
                // Задаем опции круга.
                // Включаем возможность перетаскивания круга.
                draggable: false,
                // Цвет заливки.
                // Последний байт (77) определяет прозрачность.
                // Прозрачность заливки также можно задать используя опцию "fillOpacity".
                fillColor: "#DB709377",
                // Цвет обводки.
                strokeColor: "#990066",
                // Прозрачность обводки.
                strokeOpacity: 0.8,
                // Ширина обводки в пикселях.
                strokeWidth: 5
            });

            // Добавляем круг на карту.
            //myMap.geoObjects.add(myCircle);

        });

    }
</script>