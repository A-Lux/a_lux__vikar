@extends('layouts.main')

@section('content')

    <main class="main">
        <div class="container">
            <div class="catalog-main-title">
                <h3>Товары <span>и услуги</span></h3>
            </div>

            <div style="background-image: url('/images/catalog-bg.png'); background-repeat: no-repeat; -webkit-background-size: 100% 100%; background-size: 100%; background-position: center center;"
                 class="main-content">
                <ul class="tree-branches">
                    @foreach($uslugi as $k => $v)
                        <li class="wow @if($k >= 7) echo 'zoomInLeft'; @else echo 'zoomInRight'; @endif">
                            <a href="/product/{{$v->url}}" style="text-decoration: none;color: #fff;"><p>{{$v->name}}</p></a>
                            <img src="images/triangle.png" alt="">
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </main>
    <!--END MAIN-->
    </div>

@endsection