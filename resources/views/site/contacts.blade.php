@extends('layouts.main')

@section('content')

<div class="content">

    <div class="content">
        <!--MAIN-->
        <main class="main">
            <div class="container">
                <div class="contact-main-left-title">
                    <h3 class="text-center">Контакты</h3>
                </div>
                <div class="contact-main-content">
                    <div class="col-sm-12">
                        <div class="phone-num">
                            <h3>{{$model->phone}}</h3>
                        </div>
                        <div class="email">
                            <p>{{$model->email}}</p>
                        </div>
                        <div class="contact-header">
                            <h5>{{$model->grafic}}</h5>
                        </div>
                        <div class="contact-text text-center">
                            <p>{{$model->grafic_opisanie}}</p>
                        </div>
                        <div class="address">
                            <p>{{$model->address}}</p>
                        </div>
                        <div class="map">
                            <iframe src="{{$model->map}}" width="100%" height="565px" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!--END MAIN-->
    </div>

@endsection