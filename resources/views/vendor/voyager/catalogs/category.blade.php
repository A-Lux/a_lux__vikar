<label for="name">Для цены</label>
@foreach($calcs as $v)
    <div>
        @php
            $product_calc = App\ProductsCalctwo::where('product_id', $product_id)->where('calc_id', $v->id)->first();
        @endphp
        <input type="text" name="calc[][{{$v->id}}]@if($product_calc)[{{$product_calc->id}}]@endif" value="@if($product_calc){{$product_calc->price}}@endif" style="margin-right:10px;">
        <input type="file" name="images[][{{$v->id}}]" value="@if($product_calc){{$product_calc->img}}@endif">
        @if($product_calc && $product_calc->img)
            <img src="/storage/productpriceimg/{{$model->id}}/{{$product_calc->img}}" style="width: 300px;">
        @endif
        <label>{{$v->name}}</label>
        <hr>
    </div>
@endforeach