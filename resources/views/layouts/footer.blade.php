<!-- FOOTER -->
<footer class="footer">
    <div class="container d-flex justify-content-between">
        <div class="logo-and-reserved d-flex">
            <div class="logo">
                <img src="/images/logo.png" alt="">
            </div>
            <div class="reserved">
                <p>
                    © 2018 Vikar. Все права защищены.<br>
                    Разработано веб-студией: <a href="#" target="_blank">A-Lux</a>
                </p>
            </div>
        </div>

        <div class="footer-soc-contacts d-flex">
            <div class="header-contacts pr-4">
                <p><a href="tel:+7 (727) 739-335-79">+7 (727) 739-335-79</a></p>
                <p>Email: <a href="mailto: vikarsupport@mail.ru">vikarsupport@mail.ru</a></p>
            </div>

            <div class="footer-socials">
                <ul>
                    <li><a href="#"><img src="/images/soc-1.png" alt=""></a></li>
                    <li><a href="#"><img src="/images/soc-2.png" alt=""></a></li>
                    <li><a href="#"><img src="/images/soc-3.png" alt=""></a></li>
                    <li><a href="#"><img src="/images/soc-4.png" alt=""></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER -->

{{--@if(!isset($jquery))--}}
{{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"--}}
        {{--crossorigin="anonymous"></script>--}}
{{--@endif--}}
{{--<script src="/js/jquery.mask.min.js"></script>--}}
{{--<script src="/js/owl.carousel.min.js"></script>--}}
{{--<script src="/js/wow.min.js"></script>--}}
{{--<script src="/js/main.js"></script>--}}
{{--<script src="https://unpkg.com/axios/dist/axios.min.js"></script>--}}
{{--<script src="/js/vue.js"></script>--}}
{{--<script src="/js/vue_scripts.js"></script>--}}
<script src="/js/jquery.min.js"></script>
<script src="/js/jquery.magnific-popup.min.js"></script>
<script src="/js/jquery.mask.min.js"></script>
<script src="/js/fotorama.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/wow.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@glidejs/glide"></script>
<script src="/js/main.js"></script>
</body>
</html>