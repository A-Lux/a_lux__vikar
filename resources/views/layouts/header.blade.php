<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>VIKAR</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/fotorama.css">
	<link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

<div class="content">
    <!--HEADER-->
    <header class="header">
        <div class="container">
            <!--HEADER-TOP-->
            <div class="header-top d-flex justify-content-between">
                <a href="/images/price-list.pdf" class="price-list d-flex">
                    <div class="psd-icon">
                        <img src="/images/psd.png" alt="">
                    </div>
                    <p>
                        Актуальные цены на услуги<br>
                        <span>Скачать прайс-лист</span>
                    </p>
                </a>
                <a href="/" class="logo">
                    <img src="/images/logo.png" alt="LOGO">
                    <p>Жарнама агенттігі</p>
                </a>

                <div class="header-contacts">
                    <p><a href="tel:+7 (727) 739-335-79">+7 (727) 739-335-79</a></p>
                    <p>Email: <a href="mailto: vikarsupport@mail.ru">vikarsupport@mail.ru</a></p>
                </div>
            </div>
            <!--END HEADER-TOP-->

            <!--HEADER MENU-->
            <nav class="nav-menu">
                <ul>
                    @foreach($menu as $v)
                        <li><a href="/{{$v->url}}">{{$v->name}}</a></li>
                    @endforeach
                </ul>
            </nav>
            <!--END HEADER MENU-->

            <!--BREAD-CRUMBS-->
            {{--<div class="bread-crumbs">
                <ul>
                    <li><a href="index.php">Главная страница</a></li>
                    <li><a href="catalog2.php">Широкоформатная печать</a></li>
                    <li><a href="#">Каталог товаров</a></li>
                </ul>
            </div>--}}
            <!--END BREAD-CRUMBS-->
        </div>
    </header>
    <!--END HEADER-->