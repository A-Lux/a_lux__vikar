@extends('layouts.main')

@section('content')

    <!--MAIN-->
    <script>
        var data_products = [];
    </script>
    @php
        $count = 0;
        $summ = 0;
        //dd(Session::get('cart'));
    //Session::flush();
    @endphp
    @if(Session::get('cart'))
        @foreach(Session::get('cart') as $k => $v)
            @php
                $product = App\Catalog::find($k);
                $count += $v[0];
                $summ += $product->price50 * $v[0];//TODO price50
            @endphp
            <script>
                data_products.push(
                    {
                        name: '{{$product->name}}',
                        text: '{{$product->text}}',
                        price: '{{$product->price50}}',
                        count: '{{$v[0]}}',
                        productid: '{{$product->id}}',
                    }
                );
            </script>
        @endforeach
    @endif
    <main class="main">
        <div class="container">
            <div class="order-wrap">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="order-title">
                            <h1>Оформить <span class="red-text">заказ</span></h1>
                        </div>
                    </div>
                </div>
                <!-- СТРОКА 1 -->
                <div class="order-content">
                    <div class="row order-top">
                        <div class="col-lg-2 col-2">
                            <h4>Название</h4>
                        </div>

                        <div class="offset-3"></div>

                        <div class="col-lg-2 col-2">
                            <h4>Количество</h4>
                        </div>

                        <div class="col-lg-2 col-2">
                            <h4>Цена за ед.</h4>
                        </div>

                        <div class="col-lg-2 col-2">
                            <h4>Цена итого</h4>
                        </div>
                    </div>
                    <!-- КОНЕЦ СТРОКА 1 -->

                    <!-- СТРОКА 2 -->
                    <app-some v-for="(item, key) in info"
                              :name="item.name"
                              :text="item.text"
                              :price="item.price"
                              :count="item.count"
                              :productid="item.productid"
                              :index="key"
                              :key="key"
                              @minus="onMinus(key)"
                              @plus="onPlus(key)"
                              {{--@onkeyup="onKeyup(key, $event)"--}}
                              @delete="onDelete(key)"
                    >

                    </app-some>
                    <!-- КОНЕЦ СТРОКА 2 -->
                </div>

                <!-- ORDER BOTTOM -->
                <div class="row order-bottom">
                    <div class="col-lg-4">
                        <h6><span class="red-text">Внимание:</span></h6>
                        <p>Заказ свыше 100 000 Тг. <br> доставка <span class="red-text">бесплатно</span> по г. Алматы</p>
                    </div>

                    <div class="col-lg-8 order-total">
                        <div class="row">
                            <div class="col-lg-3 p-0">
                                <h4>Итого заказ:</h4>
                            </div>
                            <div class="col-lg-3 itog">
                                <app-car
                                        :count="parentCount"
                                        :summ="allSumm"
                                        :info="info"
                                >

                                </app-car>
                            </div>
                            <div class="col-lg-4 order-btn">
                                <a href="#">Оформить заказ</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 p-0">
                                <h4>Итого с доставкой:</h4>
                            </div>
                            <div class="col-lg-3">
                                <h5><span class="red-text">4 750 Тг</span></h5>
                            </div>
                        </div>
                    </div>

                    <div class="row order-bottom-input">
                        <div class="offset-lg-4"></div>
                        <div class="col-lg-8 order-bottom-input-item">
                            <input placeholder="Ваш контактный номер телефона" type="text">
                            <input placeholder="Написать менеджеру" type="text">
                        </div>
                    </div>
                </div>
                <!-- END ORDER BOTTOM -->
            </div>
        </div>
    </main>
    </div>
    <!--END MAIN-->
    <script>
        // console.log(data);
        parentCountjs = <?=$count?>;
        allSummjs = <?=$summ?>;
    </script>
@endsection
